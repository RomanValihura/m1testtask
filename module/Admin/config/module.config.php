<?php
namespace Admin;

use Admin\Controller;

return [
    'router'      => include(__DIR__ . '/admin.router.config.php'),
    'controllers' => [
        'invokables' => [
            'AdminIndexController' => Controller\AdminController::class,
        ],
    ],
    'acl' => include(__DIR__ . '/admin.acl.config.php'),
    'view_manager'    => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_path_stack'      => [
            __DIR__ . '/../view',
        ],
    ],
    'module_layouts' => [
        'Admin' => 'admin/layout'
    ]
];