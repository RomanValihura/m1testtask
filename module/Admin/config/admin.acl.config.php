<?php
use Zend\Http\Request;
use Api\Acl\ApiAcl;

return [
    'resources' => [
        [
            'name'         => 'admin',
            'type'         => 'controller_resource',
            'default_role' => ApiAcl::ROLE_ADMIN,
        ],
    ],
];