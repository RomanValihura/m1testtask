<?php
return [
    'routes' => [
        'admin' => [
            'type'          => 'Literal',
            'options'       => [
                'route'    => '/admin',
                'defaults' => [
                    'controller' => 'AdminIndexController',
                    'action'     => 'index',
                ],
            ],
        ],
    ],
];