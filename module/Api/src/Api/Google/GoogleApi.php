<?php

namespace Api\Google;

use Api\Exception\ApiException;
use Api\Exception\Auth\BadIdentityException;
use Api\Exception\Auth\GoogleObtainTokenException;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Json\Json;

class GoogleApi
{
    const TOKEN_URI     = 'https://www.googleapis.com/oauth2/v4/token';
    const USER_INFO_URI = 'https://www.googleapis.com/oauth2/v1/userinfo';

    /**
     * @var array
     */
    protected $config;

    /**
     * @var string
     */
    protected $token;

    /**
     * GoogleApi constructor.
     * 
     * @param string $config
     */
    public function __construct($config)
    {
        $this->config = $config['social']['google'];
    }
    
    /**
     * @param $code
     * @return $this
     * 
     * @throws GoogleObtainTokenException
     * @throws BadIdentityException
     */
    public function setTokenByCode($code)
    {
        if(empty($code)) {
            new BadIdentityException('Bad Google verify code');
        }
        
        $httpClient = new Client(static::TOKEN_URI);

        $responseObject = $httpClient
            ->setOptions(['sslverifypeer' => false])
            ->setMethod(Request::METHOD_POST)
            ->setEncType(Client::ENC_URLENCODED)
            ->setParameterPost(
                [
                    'client_id'       => $this->config['client_id'],
                    'client_secret'   => $this->config['client_secret'],
                    'redirect_uri'    => $this->config['redirect_uri'],
                    'grant_type'      => 'authorization_code',
                    'code'            => $code
                ]
            )
            ->send();
        $response = Json::decode($responseObject->getBody());

        if(empty($response->access_token)) {
            throw new GoogleObtainTokenException($responseObject->getBody());
        }

        $this->token = $response->access_token;

        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     *
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return \stdClass
     * @throws \Exception
     */
    public function getUserInfo()
    {
        if(empty($this->token)) {
            throw new ApiException('Get info google user error');
        }

        $httpClient = new Client(static::USER_INFO_URI);
        $httpClient
            ->setOptions(['sslverifypeer' => false])
            ->setParameterGet([
                'alt'          => 'json',
                'access_token' => $this->getToken(),
            ]);

        return Json::decode(
            $httpClient->send()->getBody()
        );
    }
}