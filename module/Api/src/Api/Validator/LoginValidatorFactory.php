<?php

namespace Api\Validator;


use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\EmailAddress;

class LoginValidatorFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return InputFilter
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $filter = new InputFilter();

        $filter
            ->add([
                'name'       => 'email',
                'required'   => true,
                'validators' => [
                    ['name' => EmailAddress::class],
                    ['name' => 'not_empty'],
                ],
            ])
            ->add([
                'name'       => 'password',
                'required'   => true,
                'validators' => [
                    ['name' => 'not_empty'],
                    [
                        'name'    => 'string_length',
                        'options' => [
                            'min' => 6,
                            'max' => 30,
                        ],
                    ],
                ],
            ]);

        return $filter;
    }

}