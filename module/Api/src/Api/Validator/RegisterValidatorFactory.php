<?php

namespace Api\Validator;


use Zend\Validator\Regex;
use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\EmailAddress;

class RegisterValidatorFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return InputFilter
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $filter = new InputFilter();

        $filter
            ->add([
                'name'       => 'name',
                'required'   => true,
                'validators' => [
                    [
                        'name' => 'not_empty'
                    ],
                    [
                        'name'    => 'string_length',
                        'options' => [
                            'min' => 2,
                            'max' => 30,
                        ],
                    ],
                ],
            ])

            ->add([
                'name'       => 'email',
                'required'   => true,
                'validators' => [
                    [
                        'name' => EmailAddress::class
                    ],
                    [
                        'name' => 'not_empty'
                    ],
                ],
            ])

            ->add([
                'name'       => 'phone',
                'required'   => true,
                'validators' => [
                    [
                        'name' => Regex::class,
                        'options' => [
                            'pattern' => '/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/',
                            'messages' => [
                                'regexNotMatch' => "'%value%' is not valid phone",
                            ],
                        ],
                    ],
                ],
            ])

            ->add([
                'name'       => 'password',
                'required'   => true,
                'validators' => [
                    [
                        'name' => 'not_empty'
                    ],
                    [
                        'name'    => 'string_length',
                        'options' => [
                            'min' => 6,
                            'max' => 30,
                        ],
                    ],
                ],
            ]);

        return $filter;
    }

}