<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 25.04.16
 * Time: 12:22
 */

namespace Api\Auth;

use Doctrine\ORM\EntityManager;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AuthServiceFactory implements FactoryInterface
{
    /**
     * Create auth service
     * 
     * @param ServiceLocatorInterface $serviceLocator
     * @return AuthenticationService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $authService = new AuthenticationService();

        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get('doctrine.entitymanager.orm_default');

        /** @var \Zend\Http\PhpEnvironment\Request $request */
        $request = $serviceLocator->get('request');

        $token = $request->getHeader('token')
            ? $request->getHeader('token')->getFieldValue()
            : $request->getQuery('token');

        if($token !== null) {
            $authService->setStorage(new AuthStorage($entityManager, $token));
        }

        return $authService;
    }
}