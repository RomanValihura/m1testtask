<?php

namespace Api\Auth;

use Api\Exception\Database\ReadErrorException;
use Zend\Authentication\Adapter\AbstractAdapter;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
use Doctrine\ORM\EntityManager;
use Api\Entity\User;

class AuthAdapter extends AbstractAdapter implements AdapterInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * Sets email  and password for authentication
     * 
     * @param $email
     * @param $password
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, $email, $password)
    {
        $this->entityManager = $entityManager;
        $this->identity = $email;
        $this->credential = $password;
    }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * 
     */
    public function authenticate()
    {
        $user = $this->getUserForAuth();
        
        if($user === null) {
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, $this->getIdentity());
        }

        if(!$user->getPassword()->match($this->getCredential())) {
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, $this->getIdentity());
        }

        return new Result(Result::SUCCESS, $user);
    }

    /**
     * Find user by identity
     * 
     * @return User
     * 
     * @throws ReadErrorException
     */
    protected function getUserForAuth()
    {
        try {
            /**
             * @var User $user
             */
            $user = $this->getEntityManager()
                ->getRepository(User::class)
                ->findOneBy([
                    'email' => $this->getIdentity()
                ]);
        } catch (\Exception $e) {
            throw new ReadErrorException(
                'Unread user for authorization'
            );
        }

        return $user;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }
}