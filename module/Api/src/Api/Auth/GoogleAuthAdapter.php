<?php

namespace Api\Auth;

use Api\Exception\Database\ReadErrorException;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
use Doctrine\ORM\EntityManager;

class GoogleAuthAdapter extends AuthAdapter implements AdapterInterface
{
    /**
     * Set email for authentication
     * 
     * @param $email
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, $email)
    {
        parent::__construct($entityManager, $email, null);
    }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     *
     * @throws ReadErrorException
     */
    public function authenticate()
    {
        $user = $this->getUserForAuth();
        
        if($user === null) {
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, $this->getIdentity());
        }

        return new Result(Result::SUCCESS, $user);
    }
}