<?php

namespace Api\Auth;


use Api\Error\Error;
use Api\Exception\ApiException;
use Api\Exception\ApiUserExceptionInterface;


class NotValidInputDataException extends ApiException implements ApiUserExceptionInterface
{
    private $messages;

    /**
     * NotValidInputDataException constructor.
     *
     * @param string $messages - array of messages with describe error in format:
     *  [
     *      <field_error_name> => [
     *          <message1> => 'Oops something went wrong'
     *      ]
     *  ]
     *
     */
    public function __construct($messages)
    {
        $this->messages = $messages;

        parent::__construct();
    }

    /**
     * Array with error messages
     *
     * @return array
     */
    public function getUserMessage()
    {
        return array_map(
            function ($item) {
                return array_values($item);
            },
            $this->getMessages()
        );
    }

    /**
     * @return int
     */
    public function getHttpStatusCode()
    {
        return Error::CODE_BAD_REQUEST;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

}