<?php
namespace Api\Auth;

use Api\Exception\Auth\ErrorAuthCreateException;
use Api\Exception\Database\ReadErrorException;
use Api\Exception\Database\WriteErrorException;
use Zend\Authentication\Storage\StorageInterface;
use Doctrine\ORM\EntityManager;
use Api\Entity\Auth;
use Api\Entity\User;

class AuthStorage implements StorageInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var String
     */
    private $token;

    /**
     * AuthStorage constructor.
     * 
     * @param EntityManager $entityManager
     * @param String $token
     */
    public function __construct(EntityManager $entityManager, $token)
    {
        $this->entityManager = $entityManager;
        $this->token         = $token;
    }


    /**
     * Returns true if and only if auth missing in database
     * 
     * @return bool
     */
    public function isEmpty()
    {
        return $this->read() === null;
    }

    /**
     * Returns Auth entity object or null if auth storage empty
     * 
     * @throws ReadErrorException
     * @return \Api\Entity\Auth
     */

    public function read()
    {
        try {
            return $this->getEntityRepository()
                ->findOneBy([
                    'token' => $this->token
                ]);
        } catch (\Exception $e) {
            throw new ReadErrorException();
        }
    }

    /**
     * Create new Auth entity oin database
     *
     * @param  User $user
     *
     * @throws ErrorAuthCreateException
     *
     * @return void
     */

    public function write($user)
    {
        if($user instanceof User === false) {
            throw new ErrorAuthCreateException(
                'Write auth object not instanceof from' . User::class
            );
        }

        $auth = new Auth();
        $auth->setToken($this->token);
        $auth->setUser($user);

        $this->getEntityManager()->persist($auth);
        $this->getEntityManager()->flush();
    }

    /**
     * Clear auth from database
     *
     * @throws WriteErrorException
     * @return void
     */

    public function clear()
    {
        $auth = $this->read();

        try {
            if ($auth !== null) {
                $this->getEntityManager()->remove($auth);
                $this->getEntityManager()->flush();
            }
        } catch (\Exception $e) {
            throw new WriteErrorException();
        }
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * Return repository for Auth entity
     * 
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository(Auth::class);
    }
}