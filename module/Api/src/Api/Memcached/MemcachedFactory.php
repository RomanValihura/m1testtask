<?php

namespace Api\Memcached;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MemcachedFactory implements FactoryInterface
{

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return \Memcached
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');

        $memcached = new \Memcached();
        $memcached->addServer(
            $config['memcached']['host'],
            $config['memcached']['port']
        );

        return $memcached;
    }
}