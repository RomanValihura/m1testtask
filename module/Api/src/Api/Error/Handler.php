<?php

namespace Api\Error;

use Api\Exception\ApiLoggedExceptionInterface;
use Api\Exception\ApiUserExceptionInterface;
use Api\View\ResponseView;
use Zend\Mvc\Application;
use Zend\Mvc\MvcEvent;
use Zend\Http\PhpEnvironment\Response;

class Handler
{
    public static function onApiError(MvcEvent $e)
    {
        $error = $e->getError();

        /** @var \Throwable $exception */
        $exception = $e->getParam('exception');

        /** @var Response $response */
        $response = $e->getResponse();

        switch($error) {
            case Application::ERROR_EXCEPTION:

                if($exception instanceof ApiLoggedExceptionInterface) {
                    //logger of error can be here
                }
                
                $apiError = $exception instanceof ApiUserExceptionInterface 
                    ? new Error(
                        $exception->getHttpStatusCode(),
                        $exception->getUserMessage()
                    )
                    
                    : new Error(
                        Error::CODE_UNKNOWN_ERROR,
                        Error::TYPE_UNKNOWN_ERROR
                    );

                break;
        
            case Application::ERROR_ROUTER_NO_MATCH:
                $apiError = new Error(
                    Error::CODE_NOT_FOUND,
                    Error::NOT_FOUND_MESSAGE
                );
                break;
            
            default:
                $apiError = new Error(
                    Error::CODE_UNKNOWN_ERROR,
                    $error
                );
                break;
        }

        $e->setViewModel(
            ResponseView::fail($apiError)
        );

        $e->setResult(null);

        $response->setStatusCode(
            $apiError->getCode()
        );
    }
}