<?php

namespace Api\Error;


use Zend\Stdlib\JsonSerializable;

class Error implements JsonSerializable
{
    const EMPTY_STRING = '';
    
    const NOT_FOUND_MESSAGE       = 'Please, check you request URL';

    const TYPE_BAD_REQUEST        = 'Bad request';
    const TYPE_INTERNAL_ERROR     = 'Internal error';
    const TYPE_PERMISSION_DENIED  = 'Permission denied';
    const TYPE_NOT_FOUND          = 'Resource not found';
    const TYPE_BAD_TOKEN          = 'Bad request token';
    const TYPE_METHOD_NOT_ALLOWED = 'Method not allowed';
    const TYPE_UNKNOWN_ERROR      = 'Unknown error';

    const CODE_BAD_REQUEST        = 400;
    const CODE_INTERNAL_ERROR     = 500;
    const CODE_PERMISSION_DENIED  = 401;
    const CODE_NOT_FOUND          = 404;
    const CODE_BAD_TOKEN          = 498;
    const CODE_METHOD_NOT_ALLOWED = 405;
    const CODE_UNKNOWN_ERROR      = 406;

    private $code;
    private $message;
    private $type;

    /**
     * Error constructor.
     * @param $code
     * @param $message
     * @param $type
     */
    public function __construct(
        $code    = self::CODE_UNKNOWN_ERROR,
        $message = self::EMPTY_STRING,
        $type    = null
    )
    {
        $code = $this->checkCode($code)
            ? $code
            : self::CODE_UNKNOWN_ERROR;

        $this->code    = $code;
        $this->message = $message;

        $this->type    = $type === null
            ? $this->getTypeByCode($code)
            : $type;
    }


    public function jsonSerialize()
    {
        return [
            'type'    => $this->type,
            'code'    => $this->code,
            'message' => $this->message,
        ];
    }

    protected function getTypeByCode($code) {
        $codeTypeError = [
            self::CODE_BAD_REQUEST        => self::TYPE_BAD_REQUEST,
            self::CODE_INTERNAL_ERROR     => self::TYPE_INTERNAL_ERROR,
            self::CODE_PERMISSION_DENIED  => self::TYPE_PERMISSION_DENIED,
            self::CODE_NOT_FOUND          => self::TYPE_NOT_FOUND,
            self::CODE_BAD_TOKEN          => self::TYPE_BAD_TOKEN,
            self::CODE_METHOD_NOT_ALLOWED => self::TYPE_METHOD_NOT_ALLOWED,
            self::CODE_UNKNOWN_ERROR      => self::TYPE_UNKNOWN_ERROR,
        ];

        return isset($codeTypeError[$code])
            ? $codeTypeError[$code]
            : self::TYPE_UNKNOWN_ERROR;
    }

    private function checkCode($code)
    {
        return in_array(
            $code,
            [
                self::CODE_BAD_REQUEST,
                self::CODE_INTERNAL_ERROR,
                self::CODE_PERMISSION_DENIED,
                self::CODE_NOT_FOUND,
                self::CODE_BAD_TOKEN,
                self::CODE_METHOD_NOT_ALLOWED,
                self::CODE_UNKNOWN_ERROR,
            ]
        );
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return null|string
     */
    public function getType()
    {
        return $this->type;
    }
}