<?php
namespace Api\Entity;


interface ResourceEntityInterface
{
    const FIELD_NAME      = 'name';
    const FIELD_AVAILABLE = 'available';
    const FIELD_REFERENCE = 'reference';
    const COLLECTION_TYPE = 'collection_type';

    const READ  = 'read';
    const WRITE = 'write';
    

    function getId();

    public function getDescribeFields();
}