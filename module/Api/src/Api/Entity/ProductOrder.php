<?php

namespace Api\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductOrder
 *
 * @ORM\Table(name="t_product_order", indexes={
 *     @ORM\Index(name="product_id", columns={"product_id"}),
 *     @ORM\Index(name="order_id", columns={"order_id"})
 * })
 * @ORM\Entity
 */
class ProductOrder implements ResourceEntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var \Api\Entity\Order
     *
     * @ORM\ManyToOne(targetEntity="Api\Entity\Order")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * })
     */
    private $order;

    /**
     * @var \Api\Entity\Product
     *
     * @ORM\ManyToOne(targetEntity="Api\Entity\Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    public function getDescribeFields()
    {
        return [
            'quantity' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                    static::WRITE,
                ]
            ],

            'user' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                    static::WRITE,
                ],

                static::FIELD_REFERENCE => User::class
            ],

            'product' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                    static::WRITE,
                ],

                static::FIELD_REFERENCE => Product::class
            ]
        ];
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return ProductOrder
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return ProductOrder
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }


    /**
     * Set product
     *
     * @param \Api\Entity\Product $product
     *
     * @return ProductOrder
     */
    public function setProduct(\Api\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Api\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
