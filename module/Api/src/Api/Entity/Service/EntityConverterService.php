<?php

namespace Api\Entity\Service;

use Api\Entity\ResourceEntityInterface;
use Api\Exception\Database\WriteErrorException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;

class EntityConverterService
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ResourceEntityInterface
     */
    private $entity;

    /**
     * EntityConverterService constructor.
     * 
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager  = $entityManager;
    }

    /**
     * @return ResourceEntityInterface
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param $entity
     * 
     * @return $this
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
        
        return $this;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * Convert doctrine entity to array
     *
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach ($this->getEntity()->getDescribeFields() as $fieldName => $fieldConfig) {

            //if field not available to read, then skip it
            if(!in_array(ResourceEntityInterface::READ, $fieldConfig[ResourceEntityInterface::FIELD_AVAILABLE])) {
                continue;
            }

            $getter = 'get' . ucfirst($fieldName);

            if(is_callable([$this->getEntity(), $getter])) {
                $fieldValue = $this->getEntity()->$getter();

                //if nest field is other resource entity, then covert it
                if($fieldValue instanceof ResourceEntityInterface) {
                    $converter = clone $this;
                    $data[$fieldName] = $converter->setEntity($fieldValue)->toArray();
                } elseif($fieldValue instanceof PersistentCollection) {
                    $data[$fieldName] = array_map(
                        function (ResourceEntityInterface $item) {
                            $converter = clone $this;
                            return $converter->setEntity($item)->toArray();
                        },
                        $fieldValue->toArray()
                    );
                } else {
                    $data[$fieldName] = $fieldValue;
                }
            }
        }
        
        return $data;
    }

    /**
     * Create doctrine entity form array
     *
     * @param $data - array data
     * @param $entityClass - if not null, then current class entity will be override
     *
     * @return $this
     * @throws WriteErrorException
     */
    public function toEntityObject($data, $entityClass = null)
    {
        if(is_subclass_of($entityClass, ResourceEntityInterface::class)) {
            $this->setEntity(new $entityClass);
        }

        if($this->getEntity() === null) {
            throw new WriteErrorException('Failed convert field to object');
        }

        foreach($this->getEntity()->getDescribeFields() as $fieldName => $fieldConfig) {
            if(!in_array(ResourceEntityInterface::WRITE, $fieldConfig[ResourceEntityInterface::FIELD_AVAILABLE])) {
                continue;
            }

            $setter = 'set' . ucfirst($fieldName);

            if(is_callable([$this->getEntity(), $setter]) && isset($data[$fieldName])) {
                if(!empty($fieldConfig[ResourceEntityInterface::FIELD_REFERENCE])) {
                    $data[$fieldName] = $this->createReferenceEntity(
                        $fieldConfig[ResourceEntityInterface::FIELD_REFERENCE],
                        $data[$fieldName]
                    );
                }

                //if field is collection, then skip. Collection will be apply after persist their parent
                if(!empty($fieldConfig[ResourceEntityInterface::COLLECTION_TYPE])) {
                    continue;
                }

                $this->getEntity()->$setter($data[$fieldName]);
            }
        }

        return $this;
    }

    protected function createReferenceEntity($refEntityName, $refEntityContent)
    {
        $entity = $this->getEntityManager()->getReference(
            $refEntityName,
            (int)$refEntityContent['id']
        );


        return $entity;
    }

    /**
     * Create nest collection for entity
     * For correct work this method, parent collection must be persist
     *
     * @param $data
     *
     * @throws WriteErrorException
     */
    public function applyNestCollection($data)
    {
        foreach($this->getEntity()->getDescribeFields() as $fieldName => $fieldConfig) {
            if(empty($fieldConfig[ResourceEntityInterface::COLLECTION_TYPE])) {
                continue;
            }

            $collectionAdd = 'add' . ucfirst($fieldName);

            if(is_callable([$this->getEntity(), $collectionAdd]) && isset($data[$fieldName])) {

                $collectionEntities = $data[$fieldName];
                $collectionType = $fieldConfig[ResourceEntityInterface::COLLECTION_TYPE];


                foreach($collectionEntities as $collectionEntity) {
                    $converter = clone $this;

                    $entity = $converter->setEntity(null)->toEntityObject($collectionEntity, $collectionType)->getEntity();

                    $this->getEntity()->$collectionAdd($entity);

                    $this->getEntityManager()->persist($entity);
                    $this->getEntityManager()->flush();
                }
            }
        }
    }
}