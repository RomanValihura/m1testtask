<?php

namespace Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 *
 * @ORM\Table(name="t_order", indexes={@ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class Order implements ResourceEntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=false)
     */
    private $updatedDate;

    /**
     * @var \Api\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Api\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Api\Entity\ProductOrder", mappedBy="order")
     */
    private $orderProducts;


    public function getDescribeFields()
    {
        return [
            'id' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                ],
            ],

            'orderProducts' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                    static::WRITE,
                ],

                static::COLLECTION_TYPE => ProductOrder::class
            ],

            'user' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                    static::WRITE,
                ],

                static::FIELD_REFERENCE => User::class
            ],

            'createdDate' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                ],
            ],

            'updatedDate' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                ],
            ],
        ];
    }

    public function __construct()
    {
        $this->createdDate= new \DateTime();
        $this->updatedDate= new \DateTime();
        $this->orderProducts = new ArrayCollection();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedDate= new \DateTime();
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return string
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * @param string $updatedDate
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;
    }


    /**
     * @return mixed
     */
    public function getOrderProducts()
    {
        return $this->orderProducts;
    }

    /**
     * @param ProductOrder $productOrder
     */
    public function addOrderProducts(ProductOrder $productOrder)
    {
        $productOrder->setOrder($this);
        $this->orderProducts->add($productOrder);
    }
}

