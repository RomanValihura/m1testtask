<?php

namespace Api\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="t_product", indexes={@ORM\Index(name="name_i", columns={"name"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Product implements ResourceEntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity_on_stock", type="integer", nullable=false)
     */
    private $quantityOnStock;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=false)
     */
    private $updatedDate;

    
    
    public function getDescribeFields()
    {
        return [
            'id' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                ]
            ],

            'name' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                    static::WRITE,
                ]
            ],

            'description' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                    static::WRITE,
                ]
            ],

            'price' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                    static::WRITE,
                ]
            ],

            'quantityOnStock' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                    static::WRITE,
                ]
            ],

            'createdDate' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                ]
            ],

            'updatedDate' => [
                static::FIELD_AVAILABLE => [
                    static::READ,
                ]
            ],
        ];
    }

    public function __construct()
    {
        $this->createdDate= new \DateTime();
        $this->updatedDate= new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedDate= new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set quantityOnStock
     *
     * @param integer $quantityOnStock
     *
     * @return Product
     */
    public function setQuantityOnStock($quantityOnStock)
    {
        $this->quantityOnStock = $quantityOnStock;

        return $this;
    }

    /**
     * Get quantityOnStock
     *
     * @return integer
     */
    public function getQuantityOnStock()
    {
        return $this->quantityOnStock;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return Product
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set updatedDate
     *
     * @param \DateTime $updatedDate
     *
     * @return Product
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * Get updatedDate
     *
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }
}
