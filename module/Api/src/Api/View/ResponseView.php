<?php
namespace Api\View;

use Zend\View\Model\JsonModel;

class ResponseView
{
    public static function success($result = null) {
        $response = [
            "success" => true,
        ];

        if($result !== null) {
            $response['result'] = $result;
        }

        return new JsonModel($response);
    }

    public static function successList($result = [], $total) {
        $response = [
           "success" => true,
            "total" => $total,
            "result" => $result
        ];

        return new JsonModel($response);
    }

    public static function fail($error) {
        return new JsonModel([
            "success" => false,
            "error" => $error
        ]);
    }
}