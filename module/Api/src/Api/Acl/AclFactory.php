<?php

namespace Api\Acl;

use Api\Exception\Acl\MissingDefaultRoleException;
use Zend\Http\Request;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AclFactory implements FactoryInterface
{
    /**
     * @var Acl
     */
    protected $acl;

    /**
     * AclFactory constructor.
     */
    public function __construct()
    {
        $this->acl = new Acl();
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return Acl
     * 
     * @throws MissingDefaultRoleException
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        $aclConfig = $config['acl'];

        $this->addRoles($aclConfig['roles']);
        $this->addResources($aclConfig['resources']);

        return $this->acl;
    }

    /**
     * Add role to ACL
     * 
     * @param $roles
     */
    protected function addRoles($roles)
    {
        foreach($roles as $role => $parentRoles) {
            $this->acl->addRole(new Role($role), $parentRoles);
        }
    }

    /**
     * Add resource to ACL
     * 
     * @param $resources
     * 
     * @throws MissingDefaultRoleException
     */
    protected function addResources($resources)
    {
        foreach($resources as $resource) {
            foreach ($this->getAvailableMethod() as $method) {
                $this->acl->addResource(
                    new Resource(
                        ApiAcl::getFullResourceName(
                            $resource['name'],
                            $method
                        )
                    )
                );
            }

            if (empty($resource['default_role'])) {
                throw new MissingDefaultRoleException($resource['name']);
            }

            $this->assignRoleToResource($resource);
        }
    }

    /**
     * @param $resource
     */
    protected function assignRoleToResource($resource)
    {
        foreach ($this->getAvailableMethod() as $method) {
            $roleIsSpecial = isset($resource['methods_role']) && !empty($resource['methods_role'][$method]);

            $this->acl->allow(
                $roleIsSpecial
                    ? $resource['methods_role'][$method] //if isset special role for http method, then set it
                    : $resource['default_role'],         //else set default resource role
                ApiAcl::getFullResourceName(
                    $resource['name'],
                    $method
                )
            );
        }
    }

    /**
     * Available method for set specific permission
     * 
     * @return array
     */
    public function getAvailableMethod()
    {
        return [
            Request::METHOD_GET,
            Request::METHOD_POST,
            Request::METHOD_PUT,
            Request::METHOD_DELETE,
        ];
    }
}