<?php
/**
 * Created by PhpStorm.
 * User: rvalihura
 * Date: 22.04.16
 * Time: 18:36
 */

namespace Api\Acl;

use Api\Entity\Auth;
use Api\Error\Handler;
use Api\Exception\Acl\NotAllowedException;
use Api\Exception\AclException;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Application;
use Zend\Mvc\MvcEvent;

class ApiAcl
{
    const ROLE_GUEST = 'guest';
    const ROLE_USER  = 'user';
    const ROLE_ADMIN = 'admin';

    private function __construct()
    {
        //deny create object this class type
    }

    /**
     * Check user permission on resource 
     * and throw Exception if resource not allowed
     * 
     * @param MvcEvent $e
     */
    public static function checkAccess(MvcEvent $e)
    {
        $currentRole = self::detectRole($e);

        $resourceName = self::getFullResourceName(
            $e->getRouteMatch()->getMatchedRouteName(),
            $e->getRequest()->getMethod()
        );

        $acl = $e->getApplication()->getServiceManager()->get('acl_service');

        //if method not allowed, then manual receive exception in handler
        if(!$acl->isAllowed($currentRole, $resourceName)) {
            $e->setError(Application::ERROR_EXCEPTION);
            $e->setParam(
                'exception',
                new NotAllowedException()
            );
            Handler::onApiError($e);
        }
    }

    /**
     * Return role for current user
     * 
     * @param MvcEvent $e
     * @return string
     */
    protected static function detectRole(MvcEvent $e)
    {
        /** @var AuthenticationService $entityManager */
        $authService = $e->getApplication()
            ->getServiceManager()
            ->get('auth_service');

        $identity = $authService->getIdentity();

        if ($identity instanceof Auth) {
            return $identity->getUser()->getRole();
        }

        return ApiAcl::ROLE_GUEST;
    }


    /**
     * Return resource name that consist 
     * of $resource name and current http method
     * 
     * @param $resourceName
     * @param $httpMethod
     * @return string
     */
    public static function getFullResourceName($resourceName, $httpMethod)
    {
        return $resourceName . '::' . $httpMethod;
    }
}