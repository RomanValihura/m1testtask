<?php

namespace Api\Resource;

use Api\Entity;


class Product extends AbstractResource
{
    const RESOURCE_ENTITY    = Entity\Product::class;
    const PRODUCTS_CACHE_KEY = 'last_open_products';

    public function fetch($id)
    {
        $product = $this->fetchFromCache($id);


        if($product === null) {
            $product = parent::fetch($id);
            $this->putInCache($product['id']);
        }

        return $product;
    }

    protected function putInCache($data)
    {
        $cachedProducts = $this->getMemcached()->get(static::PRODUCTS_CACHE_KEY);

        if(!$cachedProducts) {
            $cachedProducts = [];
        }

        if($this->findProductInArray($cachedProducts, $data['id']) !== null) {
            return;
        }

        array_unshift(
            $cachedProducts,
            $data
        );

        $this->getMemcached()->set(
            static::PRODUCTS_CACHE_KEY,
            array_slice($cachedProducts, 0, 5),
            3000
        );
    }

    protected function fetchFromCache($id)
    {
        $cachedProducts = $this->getMemcached()->get(static::PRODUCTS_CACHE_KEY);

        if($cachedProducts) {
            $index = $this->findProductInArray($cachedProducts, $id);

            return $index === null
                ? null
                : $cachedProducts[$index];
        }

        return null;
    }

    /**
     * @param  Entity\Product[] $products
     * @param  $id
     * @return mixed
     */
    protected function findProductInArray($products, $id)
    {
        foreach ($products as $index => $product) {
            if ($product['id'] == $id) {
                return $index;
            }
        }

        return null;
    }
}