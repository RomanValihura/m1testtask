<?php

namespace Api\Resource;

use Api\Entity;

class ProductOrder extends AbstractResource
{
    const RESOURCE_ENTITY = Entity\Order::class;
}