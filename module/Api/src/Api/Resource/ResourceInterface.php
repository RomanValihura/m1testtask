<?php

namespace Api\Resource;


interface ResourceInterface
{

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param $id
     * @return \Api\Entity\ResourceEntityInterface;
     */
    public function fetch($id);

    /**
     * @param $offset
     * @param $limit
     * @param array $filter
     * @param $order
     * @return array
     */
    public function fetchAll($offset, $limit, array $filter, $order);

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, $data);

    /**
     * @param $id
     * @return void
     */
    public function delete($id);
}