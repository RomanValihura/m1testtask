<?php

namespace Api\Resource;

use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AbstractResourceFactory implements AbstractFactoryInterface
{
    const RESOURCE = 'Resource';

    const RESOURCE_NAME_INDEX = 1;
    const RESOURCE_MARK_INDEX = 0;


    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $resourceName = explode('::', $requestedName);

        return count($resourceName) === 2
            && self::RESOURCE === $resourceName[self::RESOURCE_MARK_INDEX]
            && $this->resourceExist(
                $resourceName[self::RESOURCE_NAME_INDEX]
            );

    }

    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $resourceName  = explode('::', $requestedName);

        $resource = $this->getFullResourceClass(
            $resourceName[self::RESOURCE_NAME_INDEX]
        );

        return new $resource(
            $serviceLocator->get('entity_manager'),
            $serviceLocator->get('entity_converter'),
            $serviceLocator->get('memcached')
        );
    }

    protected function getFullResourceClass($resourceName)
    {
        return __NAMESPACE__ . '\\' . ucfirst($resourceName);
    }

    protected function resourceExist($resourceShortName)
    {
        $resourceName = $this->getFullResourceClass($resourceShortName);

        return
            class_exists(
                $resourceName
            )
            && is_subclass_of(
                $resourceName,
                AbstractResource::class
            );
    }
}