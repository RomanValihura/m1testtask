<?php

namespace Api\Resource;

use Api\Entity;

class User extends AbstractResource implements ResourceInterface
{
    const RESOURCE_ENTITY = Entity\User::class;
}