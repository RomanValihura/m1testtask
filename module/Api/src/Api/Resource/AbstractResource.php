<?php
namespace Api\Resource;

use Api\Entity\ResourceEntityInterface;
use Api\Entity\Service\EntityConverterService;
use Api\Exception\Database\WriteErrorException;
use Api\Exception\Resource\NotFoundResourceException;
use Api\Exception\Database\UniqueFieldDuplicateException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;

class AbstractResource implements ResourceInterface
{
    const RESOURCE_ENTITY = null;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var EntityConverterService
     */
    protected $entityConverter;

    /**
     * @var \Memcached
     */
    protected $memcached;


    /**
     * AbstractResource constructor.
     * @param EntityManager $entityManager
     * @param EntityConverterService $entityConverter
     * @param \Memcached    $memcached
     */
    public function __construct(
        EntityManager $entityManager,
        EntityConverterService $entityConverter,
        \Memcached $memcached
    ) {
        $this->entityManager   = $entityManager;
        $this->memcached       = $memcached;
        $this->entityConverter = $entityConverter;
    }

    /**
     * @param array $data
     * 
     * @return array
     * 
     * @throws UniqueFieldDuplicateException
     * @throws WriteErrorException
     */
    public function create(array $data)
    {
        $entity = $this->getEntityConverter()
            ->toEntityObject($data, static::RESOURCE_ENTITY)
            ->getEntity();


        $this->applyEntity($entity);

        $this->getEntityConverter()->applyNestCollection($data);

        return [
            'id' => $entity->getId()
        ];
    }

    /**
     * Fetch resource by id
     * 
     * @param $id
     * @return array
     * 
     * @throws NotFoundResourceException
     */
    public function fetch($id)
    {
        $entity = $this->getEntityManager()
            ->getRepository(static::RESOURCE_ENTITY)
            ->findOneBy([
                'id' => $id
            ]);
        
        if($entity === null) {
            throw new NotFoundResourceException();
        }

        return $this->getEntityConverter()
            ->setEntity($entity)
            ->toArray();
    }

    /**
     * Fetch resource list by offset and limit 
     * 
     * @param $limit
     * @param $offset
     * @param array $filter
     * @param array $order
     * 
     * @return mixed
     */
    public function fetchAll($limit, $offset, array $filter = [], $order = ['id' => 'ASC'])
    {
        $entities = $this->getEntityManager()
            ->getRepository(static::RESOURCE_ENTITY)
            ->findBy(
                $filter,
                $order,
                $limit,
                $offset
            );

        return array_map(
            function (ResourceEntityInterface $entity) {
                return $this->getEntityConverter()
                    ->setEntity($entity)
                    ->toArray();
            },
            $entities
        );
    }

    /**
     * Update resource by id and new data
     * 
     * @param $id
     * @param array $data
     * 
     * @return array
     * 
     * @throws NotFoundResourceException
     * @throws UniqueFieldDuplicateException
     * @throws WriteErrorException
     */
    public function update($id, $data)
    {
        $entity = $this->getEntityById(static::RESOURCE_ENTITY, $id);

        if($entity === null) {
            throw new NotFoundResourceException();
        }

        $entity = $this->getEntityConverter()
            ->setEntity($entity)
            ->toEntityObject($data)
            ->getEntity();

        $this->applyEntity($entity);

        return [
            'id' => $entity->getId()
        ];
    }

    /**
     * Delete resource by id
     * 
     * @param $id
     * 
     * @throws NotFoundResourceException
     */
    public function delete($id)
    {
        $entity = $this->getEntityById(static::RESOURCE_ENTITY, $id);

        if($entity === null) {
            throw new NotFoundResourceException();
        }

        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    /**
     * Return total count resources in database
     * 
     * @return int
     */
    public function getTotal()
    {
        return (int)$this->getEntityManager()
            ->getRepository(static::RESOURCE_ENTITY)
            ->createQueryBuilder('e')
            ->select('count(e.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param ResourceEntityInterface $entity
     * 
     * @throws UniqueFieldDuplicateException
     * @throws WriteErrorException
     */
    protected function applyEntity(ResourceEntityInterface $entity)
    {
        try {
            $this->getEntityManager()->persist($entity);
            $this->getEntityManager()->flush($entity);

        } catch (UniqueConstraintViolationException $e) {
            throw new UniqueFieldDuplicateException();
        } catch (\Exception $e) {
            throw new WriteErrorException($e->getMessage());
        }
    }

    /**
     * Find entity by id
     * 
     * @param $entityClass
     * @param $id
     * @return null|object
     */
    protected function getEntityById($entityClass, $id)
    {
        return $this->getEntityManager()
            ->getRepository($entityClass)
            ->findOneBy(['id' => $id]);
    }

    /**
     * Return entity class name Entity class
     * 
     * @return string
     */
    protected function getResourceEntityName()
    {
        return static::RESOURCE_ENTITY;
    }

    /**
     * @return \Memcached
     */
    public function getMemcached()
    {
        return $this->memcached;
    }

    /**
     * @return EntityConverterService
     */
    public function getEntityConverter()
    {
        return $this->entityConverter;
    }
}