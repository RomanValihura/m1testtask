<?php
namespace Api\Controller;

use Api\Exception\Resource\NotSupportedMethod;
use Api\Resource\AbstractResource;
use Api\View\ResponseView;
use Zend\Http\Request;
use Zend\Http\Response;


class IndexController extends AbstractController
{
    const ROUTE_DELIMITER = '/';

    /**
     * @var AbstractResource
     */
    private $resource;



    public function indexAction()
    {
        $this->resource = $this->getServiceLocator()->get($this->getResourceName());

        switch($this->getRequest()->getMethod()) {
            case Request::METHOD_GET:
                return $this->processGet();

            case Request::METHOD_POST:
                return $this->processPost();
            
            case Request::METHOD_PUT:
                return $this->processPut();
            
            case Request::METHOD_DELETE:
                return $this->processDelete();
                
            default:
                throw new NotSupportedMethod;
        }
    }

    protected function processGet()
    {
        $resourceId = $this->getRoute()->getParam('id');

        if($resourceId !== null) {
            return ResponseView::success(
                $this->getResource()->fetch($resourceId)
            );
        } else {
            $data = $this->getResource()->fetchAll(
                (int)$this->getRequest()->getQuery('limit')  ?: 50,
                (int)$this->getRequest()->getQuery('offset') ?: 0
            );
            return ResponseView::successList(
                $data, $this->getResource()->getTotal()
            );
        }
    }
    
    protected function processPost()
    {
        return ResponseView::success(
            $this->getResource()
                ->create(
                    $this->getRequestContent()
                )
        );
    }
    
    protected function processPut()
    {
        $resourceId = $this->getRoute()->getParam('id');

        return ResponseView::success(
            $this->getResource()
                ->update(
                    $resourceId,
                    $this->getRequestContent()
                )
        );
    }    
    
    protected function processDelete()
    {
        $resourceId = $this->getRoute()->getParam('id');

        $this->getResource()->delete($resourceId);

        return $this->getResponse()
            ->setStatusCode(Response::STATUS_CODE_204);
    }

    /**
     * Generate valid resource name by route match
     * 
     * @return string - prepared for resource factory resource name
     */
    protected function getResourceName()
    {
        // Split route on section
        // Last of route section is a resource name
        $mathRoute = explode(
            self::ROUTE_DELIMITER,
            $this->getRoute()->getMatchedRouteName()
        );
        
        return 'Resource::' . end($mathRoute);
    }

    /**
     * @return AbstractResource
     */
    public function getResource()
    {
        return $this->resource;
    }
}
