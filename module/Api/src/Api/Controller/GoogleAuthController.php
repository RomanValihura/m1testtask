<?php
namespace Api\Controller;

use Api\Auth\AuthStorage;
use Api\Auth\GoogleAuthAdapter;
use Api\Entity\User;
use Api\Google\GoogleApi;
use Api\View\ResponseView;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\ViewModel;


class GoogleAuthController extends AuthController
{
    /**
     * @var GoogleApi
     */
    private $googleApi;
    

    /**
     * AuthController constructor.
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->googleApi    = $serviceLocator->get('google_api');
        
        parent::__construct($serviceLocator);
    }
    
    /**
     * Google login action
     *
     * @return ViewModel
     * @throws
     */
    public function loginAction()
    {
        try {
            $code = $this->getRequest()->getQuery('code');

            $email = $this->getGoogleApi()
                ->setTokenByCode($code)
                ->getUserInfo()
                ->email;

            //if user not created - add new user
            if (!$this->issetUser(['email' => $email])) {
                $this->registerUser();
            }

            $token = $this->generateToken($email);

            //init auth service
            $this->getAuthService()
                ->setStorage(
                    new AuthStorage(
                        $this->getEntityManager(),
                        $token
                    )
                )
                //set specific adapter for check only identity
                ->setAdapter(
                    new GoogleAuthAdapter(
                        $this->getEntityManager(),
                        $email
                    )
                );

            $viewModel = $this->getAuthService()->authenticate()->isValid()
                ? $this->googleSuccessViewModel($token)
                : $this->googleErrorViewModel();

        } catch (\Exception $e) {
            //call log error can be here

            $viewModel = $this->googleErrorViewModel();
        }
        
        return $viewModel;
    }


    /**
     * Add new user with google email and generated password
     */
    protected function registerUser()
    {
        $user = $this->getGoogleApi()->getUserInfo();

        $resource = $this->getServiceLocator()->get('Resource::User');

        $resource->create(
            [
                'name'     => $user->name,
                'email'    => $user->email,
                'password' => $this->generatePassword(),
                'ip'       => $this->getRequest()->getServer('REMOTE_ADDR')
            ]
        );
    }

    /**
     * Checks whether there is a user by predicate
     *
     * @param $predicate
     * @return bool - true if user exist
     */
    protected function issetUser($predicate) {
        $user = $this->getEntityManager()
            ->getRepository(User::class)
            ->findOneBy($predicate);

        return $user !== null;
    }

    /**
     * @return ViewModel
     */
    private function googleErrorViewModel()
    {
        return new ViewModel([
            'result' => ResponseView::fail([
                'message' => 'Unknown google authorization error'
            ])
        ]);
    }

    /**
     * @param $token
     *
     * @return ViewModel
     */
    private function googleSuccessViewModel($token)
    {
        return new ViewModel([
            'result' => ResponseView::success([
                'token' => $token,
                'me'    => $this->getEntityConverter()
                    ->setEntity(
                        $this->getAuthService()
                            ->getIdentity()
                            ->getUser()
                    )
                    ->toArray()
            ])
        ]);
    }

    /**
     *
     * @return GoogleApi
     */
    protected function getGoogleApi()
    {
        return $this->googleApi;
    }

    /**
     * Generate random password
     *
     * @param int $length
     * @return mixed
     */
    protected function generatePassword($length = 8)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

        return substr(
            str_shuffle($chars), 0, $length
        );
    }
}
