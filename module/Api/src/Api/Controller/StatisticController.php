<?php

namespace Api\Controller;

use Api\Entity\Order;
use Api\Entity\Product;
use Api\Entity\ProductOrder;
use Api\Exception\Request\MissingParamException;
use Api\View\ResponseView;

class StatisticController extends AbstractController
{
    /**
     * Get statistic action
     * 
     * @return \Zend\View\Model\JsonModel
     */
    public function getAction()
    {
        return ResponseView::success([
            'totalOrders'  => $this->getEntityCount(ProductOrder::class),
            'totalProduct' => $this->getEntityCount(Product::class),
            'orderAmount'  => $this->getTotalOrderSum(),
        ]);
    }

    /**
     * User order amount action
     * 
     * @return \Zend\View\Model\JsonModel
     * 
     * @throws MissingParamException
     */
    public function userOrderAmountAction()
    {
        $userEmail = $this->getRequest()->getQuery('email');
        
        if($userEmail === null) {
            throw new MissingParamException('Missing required query parameter `email`');
        }

        return ResponseView::success([
            'orderAmount' => $this->geOrderSumForUser($userEmail)
        ]);
    }

    /**
     * Return count of entity
     * 
     * @param string $entity - entity class
     * @return int
     */
    protected function getEntityCount($entity)
    {
        return (int)$this->getEntityManager()
            ->getRepository($entity)
            ->createQueryBuilder('e')
            ->select('count(e.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }


    /**
     * Return total order sum in store
     * 
     * @return double
     */
    protected function getTotalOrderSum()
    {
        $queryBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('sum(po.quantity * p.price)')
            ->from(ProductOrder::class, 'po')
            ->join('po.product', 'p');

        return (double)$queryBuilder
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Return order sum for user with selected email
     * 
     * @param $userEmail
     * @return double
     */
    protected function geOrderSumForUser($userEmail)
    {
        $queryBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('sum(po.quantity * p.price)')
            ->from(ProductOrder::class, 'po')
            ->join('po.order', 'o')
            ->join('po.product', 'p')
            ->join('o.user', 'u')
            ->where('u.email = :email')
            ->setParameter('email', $userEmail);
        

        return (double)$queryBuilder
            ->getQuery()
            ->getSingleScalarResult();
    }
}