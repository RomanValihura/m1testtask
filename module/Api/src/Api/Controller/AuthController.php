<?php
namespace Api\Controller;

use Api\Auth\AuthAdapter;
use Api\Auth\AuthStorage;
use Api\Auth\NotValidInputDataException;
use Api\Entity\Service\EntityConverterService;
use Api\View\ResponseView;
use Zend\Authentication\AuthenticationService;
use Zend\Di\ServiceLocator;
use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\JsonModel;

class AuthController extends AbstractController
{
    /**
     * @var AuthenticationService
     */
    protected $authService;

    /**
     * @var EntityConverterService
     */
    protected $entityConverter;


    /**
     * AuthController constructor.
     * 
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->authService     = $serviceLocator->get('auth_service');
        $this->entityConverter = $serviceLocator->get('entity_converter');
        
        parent::__construct($serviceLocator);
    }
    
    /**
     * Post request with auth parameters
     *
     * @return JsonModel
     * @throws
     */
    public function loginAction()
    {
        $content = $this->getRequestContent();

        if(!$this->getLoginValidator()->setData($content)->isValid()) {
            throw new NotValidInputDataException(
                $this->getLoginValidator()->getMessages()
            );
        }

        $token = $this->generateToken($content['email']);

        //init auth service
        $this->getAuthService()
            ->setStorage(
                new AuthStorage(
                    $this->getEntityManager(),
                    $token
                )
            )
            ->setAdapter(
                new AuthAdapter(
                    $this->getEntityManager(),
                    $content['email'],
                    $content['password']
                )
            );
        

        if($this->getAuthService()->authenticate()->isValid()) {
            return ResponseView::success([
                'token' => $token,
                'me' => $this->getEntityConverter()
                    ->setEntity(
                        $this->getAuthService()
                            ->getIdentity()
                            ->getUser()
                    )
                    ->toArray()
            ]);
        } else {
            return ResponseView::fail([
                'message' => 'Not valid credential'
            ]);
        }
    }

    /**
     * Logout Action
     * 
     * @return JsonModel
     */
    public function logoutAction()
    {
        $request = $this->getRequest();

        $token = $request->getHeader('token')
            ? $request->getHeader('token')->getFieldValue()
            : $request->getQuery('token');

        $this->getAuthService()
            ->setStorage(
                new AuthStorage(
                    $this->getEntityManager(),
                    $token
                )
            )
            ->clearIdentity();

        return ResponseView::success();
    }

    /**
     * Registration Action
     *
     * @return JsonModel
     *
     * @throws NotValidInputDataException
     * @throws \Api\Exception\Request\ContentFormatException
     */
    public function registrationAction()
    {
        $resource = $this->getServiceLocator()->get('Resource::User');
        $content  = $this->getRequestContent();
        $content['ip'] = $this->getRequest()->getServer('REMOTE_ADDR');

        if(!$this->getRegisterValidator()->setData($content)->isValid()) {
            throw new NotValidInputDataException(
                $this->getRegisterValidator()->getMessages()
            );
        }

        if ($resource->create($content)) {
            return ResponseView::success();
        } else {
            return ResponseView::fail('oops registration');
        }
    }


    /**
     * Generate token based on string
     * 
     * @param $userName
     * @return string
     */
    public function generateToken($userName) {
        return hash('sha256', uniqid($userName, true));
    }


    /**
     * @return AuthenticationService
     */
    protected function getAuthService()
    {
        return $this->authService;
    }

    /**
     * @return EntityConverterService
     */
    public function getEntityConverter()
    {
        return $this->entityConverter;
    }

    /**
     * Return login validator
     *
     * @return InputFilter
     */
    protected function getLoginValidator() {
        return $this->getServiceLocator()->get('login_validator');
    }

    /**
     * Return register validator
     *
     * @return InputFilter
     */
    protected function getRegisterValidator() {
        return $this->getServiceLocator()->get('register_validator');
    }
}
