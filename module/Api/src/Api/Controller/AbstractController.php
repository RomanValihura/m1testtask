<?php

namespace Api\Controller;


use Api\Exception\Request\ContentFormatException;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Json\Exception\RuntimeException;

class AbstractController extends AbstractActionController
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;
    
    
    /**
     * UserController constructor.
     * 
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->entityManager  = $serviceLocator->get('entity_manager');
    }


    /**
     * Alias for $this->getEvent()->getRouteMatch()
     * 
     * @return null|\Zend\Mvc\Router\RouteMatch
     */
    protected function getRoute()
    {
        return $this->getEvent()->getRouteMatch();
    }

    /**
     * Set normal class type for work auto complete in IDE 
     * 
     * @return \Zend\Http\PhpEnvironment\Request
     */
    public function getRequest()
    {
        return parent::getRequest();
    }
    
    /**
     * Set normal class type for work auto complete in IDE 
     * 
     * @return \Zend\Http\PhpEnvironment\Response
     */
    public function getResponse()
    {
        return parent::getResponse();
    }

    /**
     * Get json content from request
     * 
     * @return array - decoded content
     * @throws ContentFormatException
     */
    protected function getRequestContent()
    {
        $content = $this->getRequest()->getContent();

        try {
            $decodedContent = Json::decode($content, Json::TYPE_ARRAY);
        } catch (RuntimeException $e) {
            throw new ContentFormatException($this->getRequest());
        }

        return $decodedContent;
    }

    /**
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }
}