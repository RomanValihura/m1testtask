<?php

namespace Api\Exception;


interface ApiLoggedExceptionInterface extends ApiExceptionInterface
{
    /**
     * Return extended message for put to log
     *
     * @return String
     */
    public function getLogMessage();
}