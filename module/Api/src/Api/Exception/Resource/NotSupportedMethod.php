<?php

namespace Api\Exception\Resource;


use Api\Error\Error;
use Api\Exception\ApiException;
use Api\Exception\ApiUserExceptionInterface;

class NotSupportedMethod extends ApiException implements ApiUserExceptionInterface
{
    const MESSAGE   = 'The method not supported for this resource';
    const HTTP_CODE = Error::CODE_METHOD_NOT_ALLOWED;

    /**
     * @return string
     */
    public function getUserMessage()
    {
        return static::MESSAGE;
    }

    /**
     * @return int
     */
    public function getHttpStatusCode()
    {
        return static::HTTP_CODE;
    }
}