<?php

namespace Api\Exception\Resource;


use Api\Error\Error;
use Api\Exception\ApiException;
use Api\Exception\ApiUserExceptionInterface;

class NotFoundResourceException extends ApiException implements ApiUserExceptionInterface
{
    const MESSAGE   = 'This resource not found';
    const HTTP_CODE = Error::CODE_NOT_FOUND;

    /**
     * @return string
     */
    public function getUserMessage()
    {
        return static::MESSAGE;
    }

    /**
     * @return int
     */
    public function getHttpStatusCode()
    {
        return static::HTTP_CODE;
    }
}