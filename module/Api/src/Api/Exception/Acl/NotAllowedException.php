<?php

namespace Api\Exception\Acl;


use Api\Error\Error;
use Api\Exception\ApiException;
use Api\Exception\ApiUserExceptionInterface;

class NotAllowedException extends ApiException implements ApiUserExceptionInterface
{
    /**
     * @return string
     */
    public function getUserMessage()
    {
        return 'You do not have permission on this resource';
    }

    /**
     * @return int
     */
    public function getHttpStatusCode()
    {
        return Error::CODE_METHOD_NOT_ALLOWED;
    }
}