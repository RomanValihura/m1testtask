<?php

namespace Api\Exception\Acl;


use Api\Exception\ApiException;
use Api\Exception\ApiLoggedExceptionInterface;

class MissingDefaultRoleException extends ApiException implements ApiLoggedExceptionInterface
{
    private $resourceName;

    /**
     * MissingDefaultRoleException constructor.
     * @param string $resourceName
     */
    public function __construct($resourceName)
    {
        $this->resourceName = $resourceName;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function getLogMessage()
    {
        return 'Not found default role for resource' . $this->resourceName;
    }

}