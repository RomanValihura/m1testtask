<?php

namespace Api\Exception\Database;


use Api\Exception\ApiException;
use Api\Exception\ApiLoggedExceptionInterface;

class DatabaseException extends ApiException implements ApiLoggedExceptionInterface
{
    /**
     * @return String
     */
    public function getLogMessage()
    {
        return $this->getMessage();
    }
}