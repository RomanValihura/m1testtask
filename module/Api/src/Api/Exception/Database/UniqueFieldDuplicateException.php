<?php

namespace Api\Exception\Database;

use Api\Error\Error;
use Api\Exception\ApiException;
use Api\Exception\ApiLoggedExceptionInterface;
use Api\Exception\ApiUserExceptionInterface;

class UniqueFieldDuplicateException extends ApiException implements ApiUserExceptionInterface, ApiLoggedExceptionInterface
{

    /**
     * @return string
     */
    public function getLogMessage()
    {
        return $this->getMessage();
    }

    /**
     * @return string
     */
    public function getUserMessage()
    {
        return 'Entity unique filed(s) already in database, please check your data';
    }

    /**
     * @return int
     */
    public function getHttpStatusCode()
    {
        return Error::CODE_BAD_REQUEST;
    }
}