<?php

namespace Api\Exception\Auth;

use Exception;

class GoogleObtainTokenException extends AuthErrorException
{
    const MESSAGE = 'Google authorization error';

    private $responseContent;

    /**
     * GoogleObtainTokenException constructor.
     *
     * @param string $responseContent
     * @param string $message
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($responseContent, $message = "", $code = 0, Exception $previous = null)
    {
        $this->responseContent = $responseContent;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function getLogMessage()
    {
        return 'Bad response content: ' . $this->responseContent;
    }

}