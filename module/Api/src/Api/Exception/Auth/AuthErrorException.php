<?php

namespace Api\Exception\Auth;

use Api\Error\Error;
use Api\Exception\ApiException;
use Api\Exception\ApiLoggedExceptionInterface;
use Api\Exception\ApiUserExceptionInterface;

class AuthErrorException extends ApiException implements ApiUserExceptionInterface, ApiLoggedExceptionInterface
{
    const MESSAGE = 'Some authorization error';

    /**
     * @return string
     */
    public function getLogMessage()
    {
        return static::MESSAGE;
    }

    /**
     * @return string
     */
    public function getUserMessage()
    {
        return static::MESSAGE;
    }

    /**
     * @return int
     */
    public function getHttpStatusCode()
    {
        return Error::CODE_BAD_REQUEST;
    }

}