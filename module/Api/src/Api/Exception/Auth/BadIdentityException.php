<?php

namespace Api\Exception\Auth;

use Api\Error\Error;
use Api\Exception\ApiException;
use Api\Exception\ApiUserExceptionInterface;

class BadIdentityException extends ApiException implements ApiUserExceptionInterface
{
    const MESSAGE = 'Bad authorization identity';

    /**
     * @return string
     */
    public function getUserMessage()
    {
        return static::MESSAGE;
    }

    /**
     * @return int
     */
    public function getHttpStatusCode()
    {
        return Error::CODE_BAD_REQUEST;
    }
}