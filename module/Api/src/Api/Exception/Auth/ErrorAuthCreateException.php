<?php

namespace Api\Exception\Auth;

use Api\Exception\ApiException;
use Api\Exception\ApiLoggedExceptionInterface;

class ErrorAuthCreateException extends ApiException implements ApiLoggedExceptionInterface
{

    /**
     * @return string
     */
    public function getLogMessage()
    {
        return $this->getMessage();
    }
}