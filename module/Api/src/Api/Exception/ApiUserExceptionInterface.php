<?php

namespace Api\Exception;


interface ApiUserExceptionInterface extends ApiExceptionInterface
{
    
    /**
     * Return extended message for put to log
     *
     * @return String
     */
    public function getUserMessage();

    /**
     * Return https status code for response
     *
     * @return int
     */
    public function getHttpStatusCode();
}