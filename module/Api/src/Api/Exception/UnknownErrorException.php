<?php

namespace Api\Exception;


use Api\Error\Error;
use Exception;

class UnknownErrorException extends Exception implements ApiUserExceptionInterface, ApiLoggedExceptionInterface
{
    const MESSAGE   = 'Unknown API error';
    const HTTP_CODE = Error::CODE_UNKNOWN_ERROR;

    /**
     * @var String
     */
    protected $userMessage;

    /**
     * @var String
     */
    protected $logMessage;


    /**
     * BadRequest constructor.
     * @param String $userMessage
     * @param String $logMessage
     */
    public function __construct($userMessage = '', $logMessage = '')
    {
        if($userMessage === '') {
            $userMessage = static::MESSAGE;
        }
        
        $this->userMessage = $userMessage;
        $this->logMessage  = $logMessage;

        parent::__construct();
    }

    /**
     * @return String
     */
    public function getLogMessage()
    {
        return $this->logMessage;
    }

    /**
     * @return String
     */
    public function getUserMessage()
    {
        return $this->userMessage;
    }

    /**
     * @return int
     */
    public function getHttpStatusCode()
    {
        return static::HTTP_CODE;
    }
}