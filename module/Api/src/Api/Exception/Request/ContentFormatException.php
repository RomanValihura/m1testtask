<?php

namespace Api\Exception\Request;

use Api\Error\Error;
use Api\Exception\ApiLoggedExceptionInterface;
use Api\Exception\ApiUserExceptionInterface;

class ContentFormatException extends RequestException implements ApiUserExceptionInterface, ApiLoggedExceptionInterface
{
    const MESSAGE   = 'Bad request content';
    const HTTP_CODE = Error::CODE_BAD_REQUEST;

    /**
     * @return string
     */
    public function getRequestContent()
    {
        return $this->request->getContent();
    }
}