<?php

namespace Api\Exception\Request;

use Api\Error\Error;
use Api\Exception\ApiException;
use Api\Exception\ApiUserExceptionInterface;

class MissingParamException extends ApiException implements ApiUserExceptionInterface
{
    const HTTP_CODE = Error::CODE_BAD_REQUEST;

    /**
     * @return string
     */
    public function getUserMessage()
    {
        return $this->getMessage();
    }

    /**
     * @return int
     */
    public function getHttpStatusCode()
    {
        return static::HTTP_CODE;
    }
}