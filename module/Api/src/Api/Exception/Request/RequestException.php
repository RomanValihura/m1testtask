<?php

namespace Api\Exception\Request;

use Api\Error\Error;
use Api\Exception\ApiException;
use Api\Exception\ApiLoggedExceptionInterface;
use Api\Exception\ApiUserExceptionInterface;
use Zend\Http\Request;

class RequestException extends ApiException implements ApiUserExceptionInterface, ApiLoggedExceptionInterface
{
    const MESSAGE   = 'Request is not valid';
    const HTTP_CODE = Error::CODE_BAD_REQUEST;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var String
     */
    protected $userMessage;

    /**
     * @var String
     */
    protected $logMessage;


    /**
     * BadRequest constructor.
     * @param String $userMessage
     * @param String $logMessage
     * @param Request $request
     */
    public function __construct(Request $request, $userMessage = '', $logMessage = '')
    {
        if($userMessage === '') {
            $userMessage = static::MESSAGE;
        }

        $this->request     = $request;
        $this->userMessage = $userMessage;
        $this->logMessage  = $logMessage;
        
        parent::__construct();
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Return extended message for put to log
     *
     * @return String
     */
    public function getLogMessage()
    {
        return $this->logMessage;
    }

    /**
     * Return message for display in response
     *
     * @return String
     */
    public function getUserMessage()
    {
        return $this->userMessage;
    }

    /**
     * Return https status code for user response
     *
     * @return int
     */
    public function getHttpStatusCode()
    {
        return static::HTTP_CODE;
    }
}