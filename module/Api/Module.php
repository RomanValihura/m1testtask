<?php
namespace Api;

use Api\Acl\ApiAcl;
use Api\Error\Handler;
use Zend\Mvc\MvcEvent;
use Zend\View\Model;


class Module
{
    const HIGH_PRIORITY   = 0;
    const MEDIUM_PRIORITY = 1;

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();

        $eventManager->attach(
            MvcEvent::EVENT_ROUTE,
            [ApiAcl::class, 'checkAccess']
        );

        $eventManager->attach(
            MvcEvent::EVENT_DISPATCH_ERROR,
            [Handler::class, 'onApiError'],
            self::HIGH_PRIORITY
        );

        $eventManager->attach(
            MvcEvent::EVENT_RENDER_ERROR,
            [Handler::class, 'onApiError'],
            self::MEDIUM_PRIORITY
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }
}
