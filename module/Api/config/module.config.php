<?php
namespace Api;

use Api\Entity;
use Api\Google\GoogleApi;
use Api\Memcached\MemcachedFactory;
use Api\Resource;
use Api\Validator\LoginValidatorFactory;
use Api\Validator\RegisterValidatorFactory;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mvc\Controller\ControllerManager;

return [
    'router'      => include(__DIR__ . '/api.router.config.php'),
    'controllers' => [
        'factories' => [
            'IndexController' => function (ControllerManager $cm) {
                return new Controller\IndexController(
                    $cm->getServiceLocator()
                );
            },
            'AuthController' => function (ControllerManager $cm) {
                return new Controller\AuthController(
                    $cm->getServiceLocator()
                );
            },
            'GoogleAuthController' => function (ControllerManager $cm) {
                return new Controller\GoogleAuthController(
                    $cm->getServiceLocator()
                );
            },
            'StatisticController'    => function(ControllerManager $cm) {
                return new Controller\StatisticController(
                    $cm->getServiceLocator()
                );
            },
        ],
    ],
    'view_manager'    => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        
        'template_path_stack'      => [
            __DIR__ . '/../view',
        ],

        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'service_manager' => [
        'factories'          => [
            'auth_service'       => Auth\AuthServiceFactory::class,
            'acl_service'        => Acl\AclFactory::class,
            'memcached'          => MemcachedFactory::class,
            'login_validator'    => LoginValidatorFactory::class,
            'register_validator' => RegisterValidatorFactory::class,

            'google_api'   => function (ServiceLocatorInterface $sm) {
                return new GoogleApi($sm->get('config'));
            },

            'entity_converter' => function (ServiceLocatorInterface $sm) {
                /** @var EntityManager $entityManager */
                $entityManager = $sm->get('entity_manager');

                return new Entity\Service\EntityConverterService($entityManager);
            },
        ],

        'abstract_factories' => [
            Resource\AbstractResourceFactory::class
        ],

        'aliases' => [
            'entity_manager' => 'doctrine.entitymanager.orm_default',
        ],
    ],
    'acl' => include(__DIR__ . '/api.acl.config.php'),
    'module_layouts' => [
        'Api' => 'api/layout/clear'
    ]
];