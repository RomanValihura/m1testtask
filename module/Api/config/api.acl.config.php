<?php
use Zend\Http\Request;
use Api\Acl\ApiAcl;

return [
    'roles' => [
        ApiAcl::ROLE_GUEST  => null,
        ApiAcl::ROLE_USER   => [ApiAcl::ROLE_GUEST],
        ApiAcl::ROLE_ADMIN  => [ApiAcl::ROLE_GUEST, ApiAcl::ROLE_USER]
    ],

    'resources' => [
        [
            'name'         => 'api/user',
            'type'         => 'api_resource',
            'default_role' => ApiAcl::ROLE_ADMIN,
        ],
        [
            'name'         => 'api/product',
            'type'         => 'api_resource',
            'default_role' => ApiAcl::ROLE_ADMIN,
            'methods_role' => [
                Request::METHOD_GET   => ApiAcl::ROLE_GUEST
            ],
        ],
        [
            'name'         => 'api/productOrder',
            'type'         => 'api_resource',
            'default_role' => ApiAcl::ROLE_ADMIN,
            'methods_role' => [
                Request::METHOD_POST   => ApiAcl::ROLE_USER
            ],
        ],
        [
            'name'         => 'api/auth/login',
            'type'         => 'controller_resource',
            'default_role' => ApiAcl::ROLE_GUEST,
        ],
        [
            'name'         => 'api/auth/googleCallback',
            'type'         => 'controller_resource',
            'default_role' => ApiAcl::ROLE_GUEST,
        ],
        [
            'name'         => 'api/auth/registration',
            'type'         => 'controller_resource',
            'default_role' => ApiAcl::ROLE_GUEST,
        ],
        [
            'name'         => 'api/auth/logout',
            'type'         => 'controller_resource',
            'default_role' => ApiAcl::ROLE_USER,
        ],
        [
            'name'         => 'home',
            'type'         => 'controller_resource',
            'default_role' => ApiAcl::ROLE_GUEST,
        ],
        [
            'name'         => 'api/statistic',
            'type'         => 'controller_resource',
            'default_role' => ApiAcl::ROLE_ADMIN,
        ],
        [
            'name'         => 'api/statistic/user',
            'type'         => 'controller_resource',
            'default_role' => ApiAcl::ROLE_ADMIN,
        ],
    ],
];