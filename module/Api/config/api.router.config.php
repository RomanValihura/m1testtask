<?php
return [
    'routes' => [
        'api' => [
            'type'          => 'Literal',
            'options'       => [
                'route'    => '/api',
                'defaults' => [
                    'controller' => 'IndexController',
                    'action'     => 'index',
                ],
            ],
            'may_terminate' => true,
            'child_routes'  => [
                'user'    => [
                    'type'         => 'Segment',
                    'options'      => [
                        'route'    => '/user[/:id]',
                    ],
                ],
                'product'    => [
                    'type'         => 'Segment',
                    'options'      => [
                        'route'    => '/product[/:id]',
                    ],
                ],
                'productOrder'    => [
                    'type'         => 'Segment',
                    'options'      => [
                        'route'    => '/product_order[/:id]',
                    ],
                ],
                'statistic'    => [
                    'type'         => 'Literal',
                    'options'      => [
                        'route'    => '/statistic',
                        'defaults' => [
                            'controller' => 'StatisticController',
                            'action'     => 'get',
                        ],
                    ],
                    'may_terminate' => true,
                    'child_routes'  => [
                        'user' => [
                            'type'         => 'Literal',
                            'options'      => [
                                'route'    => '/user',
                                'defaults' => [
                                    'controller' => 'StatisticController',
                                    'action'     => 'userOrderAmount',
                                ],
                            ]
                        ]
                    ],
                ],
                'auth'    => [
                    'type'         => 'Literal',
                    'options'      => [
                        'route'    => '/auth',
                        'defaults' => [
                            'controller' => 'AuthController',
                            'action'     => 'login',
                        ],
                    ],
                    'may_terminate' => true,
                    'child_routes'  => [
                        'login'    => [
                            'type'         => 'Literal',
                            'options'      => [
                                'route'    => '/login',
                                'defaults' => [
                                    'controller' => 'AuthController',
                                    'action'     => 'login',
                                ],
                            ],
                        ],
                        'registration'    => [
                            'type'         => 'Literal',
                            'options'      => [
                                'route'    => '/registration',
                                'defaults' => [
                                    'controller' => 'AuthController',
                                    'action'     => 'registration',
                                ],
                            ],
                        ],
                        'logout'    => [
                            'type'         => 'Literal',
                            'options'      => [
                                'route'    => '/logout',
                                'defaults' => [
                                    'controller' => 'AuthController',
                                    'action'     => 'logout',
                                ],
                            ],
                        ],
                        'googleCallback'        => [
                            'type'    => 'Literal',
                            'options' => [
                                'route'    => '/google-oauth-callback',
                                'defaults' => [
                                    'controller' => 'GoogleAuthController',
                                    'action'     => 'login',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];