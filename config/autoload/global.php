<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return [
    'doctrine' => [
        'connection' => [
            // default connection name
            'orm_default' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params'      => [
                    'host'     => 'localhost',
                    'port'     => '3306',
                    'user'     => 'root',
                    'password' => 'qwerty12345',
                    'dbname'   => 'shopdb',
                ],
            ],
        ],
        'driver' => [
            // defines an annotation driver with two paths, and names it `my_annotation_driver`
            'app_annotation_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    'module/Api/src/Api/Entity/',
                ],
            ],

            'orm_default' => [
                'drivers' => [
                    'Api\Entity' => 'app_annotation_driver',
                ],
            ],
        ],
    ],
    'memcached' => [
        'host' => 'localhost',
        'port' => 11211,
    ],
    'social' => [
        'google' => [
            'client_id'       => '863820261464-djs5bjo58gdh1ssbchru6pqkch9bod01.apps.googleusercontent.com',
            'client_secret'   => 'o6M2ZYVQFN0vXUKVk2tTFQGL',
            'redirect_uri'    => 'http://ec2-54-149-230-150.us-west-2.compute.amazonaws.com/api/auth/google-oauth-callback',
        ]
    ]
];
