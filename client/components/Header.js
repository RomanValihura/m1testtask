// hello-world.jsx

import React from 'react';
import AjaxClient from '../lib/AjaxClient';

import {
    Navbar,
    Nav,
    NavItem,
    NavDropdown,
    MenuItem,
    Button,
    FormGroup,
    FormControl,
    ControlLabel,
    Checkbox
} from 'react-bootstrap';


class Header extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLogged: localStorage.getItem("token") ? true : false
        };

        window.addEventListener('loginChangeState', this._onLoginChangeState);
    }

    loginToggle = (e) => {
        if(this.state.isLogged) {
            let ajaxClient = new AjaxClient('/api/auth/logout');

            ajaxClient.headers = {
                token: localStorage.getItem('token')
            };

            ajaxClient.send().then(
                response => {
                    localStorage.removeItem('token');
                    localStorage.removeItem('me');

                    window.dispatchEvent(window.app.loginEvent);
                },
                error => {
                    //todo error handling
                }
            );
        } else {
            this.props.loginRef().open();
        }
    };

    _onLoginChangeState = () => {
        this.setState({
            isLogged: localStorage.getItem("token") ? true : false
        });
    };

    openCart = () => {
        this.props.cartRef().open();
    };

    render() {
        return (
            <Navbar inverse>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#">TestTask shop</a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav pullRight>
                        {this.state.isLogged && JSON.parse(localStorage.getItem('me')).role === 'admin' ? <NavItem eventKey={2} href={"/admin?token=" + localStorage.getItem('token')}>Admin Panel</NavItem> : null}
                        {this.state.isLogged ? <NavItem onClick={this.openCart} eventKey={1} href="#">Cart</NavItem> : null}
                        <NavItem onClick={this.loginToggle} eventKey={3} href="#">{this.state.isLogged ? "Logout" : "Login"}</NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
export default Header;