import React from 'react';
import _ from  'underscore';
import AjaxClient from '../lib/AjaxClient';
import PagePagination from './PagePagination';
import Cart from './Cart';

import {Grid, Row, Table, Button} from 'react-bootstrap';

const PRODUCT_PER_PAGE = 5;

class ProductList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLogged: localStorage.getItem("token") ? true : false,
            products: [],
            total: 0
        };

        window.addEventListener('loginChangeState', this._onLoginChangeState);
    }

    componentDidMount = () => {
        this.loadPage(1); //load first page
    };

    loadPage = (page) => {
        let client = new AjaxClient('/api/product', AjaxClient.METHOD_GET),
            offset = (page - 1) * PRODUCT_PER_PAGE,
            limit  = PRODUCT_PER_PAGE;

        client.queryData = {
            offset,
            limit
        };

        client.send().then(
            response => {
                this.setState({
                    products: response.result,
                    total: response.total
                });
            },
            error => {
                console.error(error);
            }
        );
    };

    _onAddToCartClick = (e) => {
        var product = _.find(
            this.state.products,
            {
                id: parseInt(e.target.getAttribute('data-index'))
            }
        );

        if(product !== undefined) {
            Cart.addProduct(product);
            this.props.cartRef().open();
        }
     };

    _onLoginChangeState = () => {
        this.setState({
            isLogged: localStorage.getItem("token") ? true : false
        });
    };

    render() {
        return (
            <Grid>
                <Row className="show-grid">
                    <Table responsive>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Product name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th/>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.products.length > 0
                            ? _.map(this.state.products, product => {
                                return (
                                    <tr key={'product' + product.id}>
                                        <td>{product.id}</td>
                                        <td>{product.name}</td>
                                        <td>{product.description}</td>
                                        <td>{product.price}</td>
                                        <td>{product.quantityOnStock}</td>
                                        {
                                            this.state.isLogged ?
                                                <td>
                                                    <Button bsStyle="success" data-index={product.id}
                                                            onClick={this._onAddToCartClick}>
                                                        Put to cart
                                                    </Button>
                                                </td> : null
                                        }

                                    </tr>
                                );
                            })

                            : <tr><td colSpan="5" style={{textAlign: 'center'}}>Product list is empty</td></tr>
                        }
                        </tbody>
                    </Table>
                </Row>
                <Row style={{textAlign: 'center'}}>
                    <PagePagination
                        loadPage={this.loadPage}
                        numberOfPage={Math.ceil(this.state.total/ PRODUCT_PER_PAGE)}
                    />
                </Row>
            </Grid>
        )
    }
}

export default ProductList;