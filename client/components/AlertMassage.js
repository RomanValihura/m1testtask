import React from 'react';

import {Alert} from 'react-bootstrap';

class AlertMassage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alertVisible: false,
            title: '',
            text: '',
            type: 'warning'
        };

        window.addEventListener('newAlert', this.handleAlertShow);
    }

    render() {
        if (this.state.alertVisible) {
            return (
                <Alert bsStyle={this.state.type} onDismiss={this.handleAlertDismiss}>
                    <h4>{this.state.title}</h4>
                    <p>{this.state.text}</p>
                </Alert>
            );
        }

        return null;
    }

    handleAlertDismiss = () => {
        this.setState({alertVisible: false});
    };

    handleAlertShow = (e) => {
        this.setState({
            alertVisible: true,
            title: e.detail.title,
            text: e.detail.text,
            type: e.detail.type
        });
    };
}

export default AlertMassage;