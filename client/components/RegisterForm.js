import React from 'react';
import AjaxClient from '../lib/AjaxClient';
import FormValidator from '../lib/FormValidator';
import _ from 'underscore';

import {
    Button,
    FormGroup,
    FormControl,
    ControlLabel,
    HelpBlock
} from 'react-bootstrap';

class RegisterForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: '',

            name: {
                errorMsg: [],
                value: ''
            },
            email: {
                errorMsg: [],
                value: ''
            },
            phone: {
                errorMsg: [],
                value: ''
            },
            password: {
                errorMsg: [],
                value: ''
            }
        };
    }

    register = () => {
        let client   = new AjaxClient('/api/auth/registration', AjaxClient.METHOD_POST),
            email    = this.state.email.value,
            name     = this.state.name.value,
            phone    = this.state.phone.value,
            password = this.state.password.value;

        client.postData = {
            email,
            name,
            phone,
            password
        };

        client.send().then(
            response => {
                let alertEvent = new CustomEvent('newAlert', {
                    detail: {
                        title: 'Registration is success!',
                        text: 'Please log-in if you want use you account.',
                        type: 'success'
                    }
                });

                window.dispatchEvent(alertEvent);

                this.props.closeModal();
            },
            error => {
                let newState = {};
                let message = error.message !== undefined ? error.message : 'Unknown error';

                if(_.isObject(message)) {
                    _.each(message, (messages, field) => {
                        if(this.state[field] !== undefined) {
                            newState[field] = {
                                errorMsg: messages,
                                valid: 'error'
                            }
                        }
                    });
                } else {
                    newState.error = message;
                }

                this.setState(newState);
            }
        );
    };

    _onNameChange = (e) => {
        let value = e.target.value,
            valid = FormValidator.checkLenght(value, 2, 30).isValid;

        this.setState({
            error: '',

            name: {
                errorMsg: [],
                valid: valid ? 'success' : 'error',
                value
            }
        });
    };

    _onEmailChange = (e) => {
        let value = e.target.value,
            valid = FormValidator.checkEmail(value).isValid;

        this.setState({
            error: '',
            email: {
                errorMsg: [],
                valid: valid ? 'success' : 'error',
                value
            }
        });
    };

    _onPhoneChange = (e) => {
        let value = e.target.value,
            valid = FormValidator.checkLenght(value, 1, 30).isValid;

        this.setState({
            error: '',
            phone: {
                errorMsg: [],
                valid: valid ? 'success' : 'error',
                value
            }
        });
    };

    _onPasswordChange = (e) => {
        let value = e.target.value,
            valid = FormValidator.checkLenght(value, 0, 30).isValid;

        this.setState({
            error: '',

            password: {
                errorMsg: [],
                valid: valid ? 'success' : 'error',
                value
            }
        });
    };

    _getErrorMessages = fieldName => {
        return this.state[fieldName].errorMsg.length > 0
            ? _.map(this.state[fieldName].errorMsg, value => {
                return <div>{value}</div>
            })
            : null
    };

    render() {
        return (
            <form>
                <FormGroup controlId="formControlsName" validationState={this.state.name.valid}>
                    <ControlLabel>Name</ControlLabel>
                    <FormControl
                        onChange={this._onNameChange}
                        type="text"
                        placeholder="Enter your name"
                        value={this.state.name.value}
                    />
                    <HelpBlock>{this._getErrorMessages('name')}</HelpBlock>
                </FormGroup>
                <FormGroup controlId="formControlsEmail" validationState={this.state.email.valid}>
                    <ControlLabel>Email address</ControlLabel>
                    <FormControl
                        onChange={this._onEmailChange}
                        type="email"
                        placeholder="Enter email"
                        value={this.state.email.value}
                    />
                    <HelpBlock>{this._getErrorMessages('email')}</HelpBlock>
                </FormGroup>
                <FormGroup controlId="formControlsPhone" validationState={this.state.phone.valid}>
                    <ControlLabel>Phone number</ControlLabel>
                    <FormControl
                        onChange={this._onPhoneChange}
                        type="text"
                        placeholder="Enter phone"
                        value={this.state.phone.value}
                    />
                    <HelpBlock>{this._getErrorMessages('phone')}</HelpBlock>
                </FormGroup>
                <FormGroup controlId="formControlsPassword" validationState={this.state.password.valid}>
                    <ControlLabel>Password</ControlLabel>
                    <FormControl
                        onChange={this._onPasswordChange}
                        type="password"
                        placeholder="Enter password"
                        value={this.state.password.value}
                    />
                    <HelpBlock>{this._getErrorMessages('password')}</HelpBlock>
                </FormGroup>

                <FormGroup validationState={this.state.error === '' ? undefined : 'error'}>
                    <HelpBlock>{this.state.error}</HelpBlock>
                </FormGroup>

                <div className="well" style={{maxWidth: 600, margin: '0 auto 10px'}}>
                    <Button bsStyle="primary" bsSize="large" block onClick={this.register}>Register</Button>
                </div>
            </form>
        );
    }
}

export default RegisterForm;