import React from 'react';
import LoginForm from './LoginForm';
import RegisterForm from './RegisterForm';

import {
    Modal,
    Tabs,
    Tab
} from 'react-bootstrap';

class IdentityModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false
        };
    }

    close = () => {
        this.setState({ showModal: false });
    };

    open = () =>  {
        this.setState({ showModal: true });
    };


    render() {
        return (
            <div className="modal-container" style={{width: 400}}>
                <Modal show={this.state.showModal} onHide={this.close}>
                    <Tabs defaultActiveKey={1} id="identity-tab">
                        <Tab eventKey={1} title="Login">
                            <Modal.Body>
                                <LoginForm closeModal={this.close} updateGlobalAuthState={this.props.updateGlobalAuthState} />
                            </Modal.Body>
                        </Tab>
                        <Tab eventKey={2} title="Registration">
                            <Modal.Body>
                                <RegisterForm closeModal={this.close} />
                            </Modal.Body>
                        </Tab>
                    </Tabs>
                </Modal>
            </div>
        );
    }
}

export default IdentityModal;