import React from 'react';
import AjaxClient from '../lib/AjaxClient';
import FormValidator from '../lib/FormValidator';
import _ from 'underscore';

import {
    Button,
    FormGroup,
    FormControl,
    ControlLabel,
    HelpBlock
} from 'react-bootstrap';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: '',
            email: {
                errorMsg: [],
                value: ''
            },
            password: {
                errorMsg: [],
                value: ''
            }
        };
    }

    login = () => {
        let client   = new AjaxClient('/api/auth/login', AjaxClient.METHOD_POST);
        let email    = this.state.email.value;
        let password = this.state.password.value;

        client.postData = {
            email,
            password
        };

        client.send().then(
            response => {
                //todo refactor it
                localStorage.setItem('token', response.result.token);
                localStorage.setItem('me', JSON.stringify(response.result.me));
                this.props.closeModal();

                window.dispatchEvent(window.app.loginEvent);
            },
            error => {
                let newState = {};
                let message = error.message !== undefined ? error.message : 'Unknown error';

                if(_.isObject(message)) {
                    _.each(message, (messages, field) => {
                        if(this.state[field] !== undefined) {
                            newState[field] = {
                                errorMsg: messages,
                                valid: 'error'
                            }
                        }
                    });
                } else {
                    newState.error = message;
                }

                this.setState(newState);
            }
        );
    };

    loginGoogle = () => {
        let oAuthUri = 'https://accounts.google.com/o/oauth2/v2/auth?' + AjaxClient.convertQueryToString({
                client_id     : '863820261464-djs5bjo58gdh1ssbchru6pqkch9bod01.apps.googleusercontent.com',
                scope         : 'email profile',
                state         : '-',
                access_type   : 'online',
                prompt        : 'select_account',
                response_type : 'code',
                redirect_uri  : 'http://ec2-54-149-230-150.us-west-2.compute.amazonaws.com/api/auth/google-oauth-callback'
            });


        //set callback function available for call via window.opener
        window.googleAuthCallback = this.googleAuthCallback;

        this.openOAuthWindow(oAuthUri, 'Login via Google');
        this.props.closeModal();
    };

    googleAuthCallback = (response) => {
        localStorage.setItem('token', response.result.token);
        localStorage.setItem('me', JSON.stringify(response.result.me));

        window.dispatchEvent(window.app.loginEvent);
        this.oAuthWindow.close();
    };

    _onEmailChange = (e) => {
        let value = e.target.value,
            valid = FormValidator.checkEmail(value).isValid;

        this.setState({
            error: '',
            email: {
                errorMsg: [],
                valid: valid ? 'success' : 'error',
                value
            }
        });
    };

    _onPasswordChange = (e) => {
        let value = e.target.value,
            valid = FormValidator.checkLenght(value, 6, 30).isValid;

        this.setState({
            error: '',
            password: {
                errorMsg: [],
                valid: valid ? 'success' : 'error',
                value
            }
        });
    };


    render() {
        return (
            <form>
                <FormGroup controlId="formControlsEmail" validationState={this.state.email.valid}>
                    <ControlLabel>Email address</ControlLabel>
                    <FormControl
                        onChange={this._onEmailChange}
                        type="email"
                        placeholder="Enter email"
                        value={this.state.email.value}
                    />
                    <HelpBlock>{this._getErrorMessages('email')}</HelpBlock>
                </FormGroup>
                <FormGroup controlId="formControlsPassword" validationState={this.state.password.valid}>
                    <ControlLabel>Password</ControlLabel>
                    <FormControl
                        onChange={this._onPasswordChange}
                        type="password"
                        placeholder="Password"
                        value={this.state.password.value}
                    />
                    <HelpBlock>{this._getErrorMessages('password')}</HelpBlock>
                </FormGroup>

                <FormGroup validationState={this.state.error === '' ? undefined : 'error'}>
                    <HelpBlock>{this.state.error}</HelpBlock>
                </FormGroup>

                <div className="well" style={{maxWidth: 600, margin: '0 auto 10px'}}>
                    <Button bsStyle="primary" bsSize="large" block onClick={this.login}>Login</Button>
                    <Button bsStyle="danger" bsSize="large" block onClick={this.loginGoogle}>Login via Google+</Button>
                </div>
            </form>
        );
    }

    _getErrorMessages = fieldName => {
        return this.state[fieldName].errorMsg.length > 0
            ? _.map(this.state[fieldName].errorMsg, value => {
                return <div>{value}</div>
            })
            : null
    };

    openOAuthWindow = (url, title) => {
        var width = screen.width / 2,
            height = screen.height / 1.5,
            left = ((screen.width - width) / 2),
            top = ((screen.height - height) / 3);

        var windowSpecs = {
            scrollbars: 'no',
            width: width,
            height: height,
            top: top,
            left: left
        };

        this.oAuthWindow = window.open(url, title,
            _.map(windowSpecs, function (value, param) {
                return param + '=' + value;
            }).join(',')
        );


        if (window.focus) {
            this.oAuthWindow.focus();
        }
    };
}

export default LoginForm;