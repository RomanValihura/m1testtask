import React from 'react';
import AjaxClient from '../lib/AjaxClient';
import FormValidator from '../lib/FormValidator';
import _ from 'underscore';

import {
    Modal,
    ListGroup,
    FormControl,
    ListGroupItem,
    ButtonToolbar,
    Button
} from 'react-bootstrap';

class Cart extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            cartItems: Cart.getProducts()
        };
    }

    setProductQuantity = (id, quantity) => {
        var cart = Cart.getProducts();

        var productIndex = _.findIndex(cart, {id});

        if(productIndex !== -1) {
            cart[productIndex].quantity = quantity;
        }

        localStorage.setItem(
            'cart',
            JSON.stringify(cart)
        );

        this.setState({
            cartItems: Cart.getProducts()
        });
    };

    static addProduct = (product, quantity = 1) => {
        var cart = Cart.getProducts();

        var productIndex = _.findIndex(cart, {id: product.id});

        if(productIndex === -1) {
            cart.push({
                id: product.id,
                product,
                quantity
            });
        } else {
            cart[productIndex].quantity++;
        }

        localStorage.setItem(
            'cart',
            JSON.stringify(cart)
        )
    };

    static removeProduct = (id) => {
        var cart = Cart.getProducts();

        localStorage.setItem(
            'cart',
            JSON.stringify(
                _.without(cart, _.findWhere(cart, {id}))
            )
        )
    };

    static getProduct = (id) => {
        return _.find(Cart.getProducts(), {id});
    };

    static getProducts = () => {
        var cart = JSON.parse(localStorage.getItem('cart'));

        return _.isArray(cart) === true ? cart : [];
    };

    clearCart = () => {
        localStorage.removeItem('cart');

        this.setState({
            cartItems: Cart.getProducts()
        });
    };

    _makeOrder = () => {
        let client = new AjaxClient('/api/product_order', AjaxClient.METHOD_POST);

        client.postData = {
            orderProducts: Cart.getProducts(),
            user: JSON.parse(localStorage.getItem('me'))
        };

        client.headers = {
            token: localStorage.getItem('token')
        };

        client.send().then(
            response => {
                let alertEvent = new CustomEvent('newAlert', {
                    detail: {
                        title: 'You order made!',
                        text: 'Please wait, we call to you.',
                        type: 'success'
                    }
                });

                window.dispatchEvent(alertEvent);
            },
            error => {
                let alertEvent = new CustomEvent('newAlert', {
                    detail: {
                        title: 'Something went wrong :(',
                        text: 'We already logged this error.',
                        type: 'error'
                    }
                });

                window.dispatchEvent(alertEvent);
            }
        );

        this.clearCart();
        this.close();
    };

    close = () => {
        this.setState({ showModal: false });
    };

    open = () =>  {
        this.setState({
            showModal: true,
            cartItems: Cart.getProducts()
        });
    };

    _onChangeProductQuantity = (e) => {
        var quantity = parseInt(e.target.value, 10),
            id = parseInt(e.target.getAttribute('data-index'), 10);

        if(quantity < 1 || _.isNaN(quantity) || _.isNaN(id)) {
            return;
        }

        this.setProductQuantity(id, quantity)
    };

    render() {
        return (
            <div className="modal-container" style={{width: 400}}>
                <Modal show={this.state.showModal} onHide={this.close}>
                    <Modal.Header>
                        <Modal.Title>Your cart</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <ListGroup>
                            {
                                this.state.cartItems.length > 0
                                    ? _.map(this.state.cartItems, item => {
                                        return <ListGroupItem key={"cart-product" + item.id}>
                                            <span>{item.product.name}</span>
                                            <input
                                                className="cart-quantity"
                                                type="number"
                                                data-index={item.id}
                                                value={item.quantity}
                                                onChange={this._onChangeProductQuantity}
                                            />
                                        </ListGroupItem>
                                    })
                                    : <ListGroupItem>You do not have a products in cart</ListGroupItem>
                            }
                        </ListGroup>
                        <ButtonToolbar>
                            <Button bsStyle="primary" disabled={this.state.cartItems.length == 0} onClick={this._makeOrder}>
                                Make order
                            </Button>
                            <Button onClick={this.clearCart} >
                                Clear cart
                            </Button>
                        </ButtonToolbar>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

export default Cart;