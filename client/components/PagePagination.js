import React from 'react';

import {Pagination} from 'react-bootstrap';

class PagePagination extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activePage: 1
        };
    }

    handleSelect = (eventKey) => {
        this.props.loadPage(eventKey);
        this.setState({
            activePage: eventKey
        });
    };

    render() {
        return (
            <Pagination
                bsSize="large"
                items={this.props.numberOfPage}
                activePage={this.state.activePage}
                onSelect={this.handleSelect}
            />
        );
    }
}

export default PagePagination;