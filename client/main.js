import ReactDOM from 'react-dom';
import React from 'react';
import App from './App';

window.app = {};

window.app.loginEvent = new Event('loginChangeState');

ReactDOM.render(
    <App />,
    document.getElementById('target')
);