// var ClosureCompilerPlugin = require('closure-compiler-webpack-plugin');

module.exports = {
    entry: "./main.js",
    output: {
        filename: "../public/bundle.js"
    },
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel', // 'babel-loader' is also a legal name to reference
                query: {
                    presets: ['es2015', 'stage-0', 'react']
                }
            }
        ]
    },
    resolve: {
        extensions: ["", ".js"]
    }
    // ,
    // plugins: [
    //     new ClosureCompilerPlugin()
    // ]
};