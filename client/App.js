import React from 'react';
import Header from './components/Header';
import IdentityModal from './components/IdentityModal'
import Cart from './components/Cart'
import AlertMessage from './components/AlertMassage'
import ProductList from './components/ProductList'


class App extends React.Component {
    getLoginRef = () => {
        return this.identityModal;
    };

    getCartRef = () => {
        return this.cartRef;
    };

    render() {
        return (
            <div>
                <IdentityModal ref={ref => this.identityModal = ref} />
                <Cart ref={ref => this.cartRef = ref}  />
                <Header loginRef={this.getLoginRef} cartRef={this.getCartRef} />
                <AlertMessage />
                <ProductList cartRef={this.getCartRef} />
            </div>
        )
    }
}

export default App;