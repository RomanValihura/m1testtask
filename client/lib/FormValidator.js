const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class FormValidator {
    /* Class constants */
    static get TOO_LONG_ERROR  () { return "TOO_LONG_ERROR"  };
    static get TOO_SHORT_ERROR () { return "TOO_SHORT_ERROR" };
    static get NOT_VALID_ERROR () { return "NOT_VALID_ERROR" };

    static checkEmail (email) {
        let error = '',
            isValid = true;

        if(email.length > 90) {
            error = FormValidator.TOO_LONG_ERROR;
            isValid = false;
        }

        if(!EMAIL_REGEX.test(email)) {
            error = FormValidator.NOT_VALID_ERROR;
            isValid = false;
        }
        
        return {
            error,
            isValid
        }
    }

    static checkLenght (field, min, max) {
        let error = '',
            isValid = true;

        if(field.length > max) {
            error = FormValidator.TOO_LONG_ERROR;
            isValid = false;
        }

        if(field.length < min) {
            error = FormValidator.TOO_SHORT_ERROR;
            isValid = false;
        }

        return {
            error,
            isValid
        }
    }
}

export default FormValidator;