import _ from 'underscore';

class AjaxClient {
    constructor (url, method = AjaxClient.METHOD_GET) {
        this._url       = url;
        this._method    = method;
        this._headers   = [];
        this._postData  = [];
        this._queryData = [];

        this._request = new XMLHttpRequest();
    }

    /* Class constants */
    static get METHOD_GET    () { return "GET"   };
    static get METHOD_POST   () { return "POST"  };
    static get METHOD_PUT    () { return "PUT"   };
    static get METHOD_DELETE () { return "DELETE"};

    set url (url) {
        this._url = url;
    }

    get url () {
        return this._url;
    }

    set headers (headers) {
        this._headers = headers;
    }

    get headers () {
        return this._headers;
    }

    set queryData (data) {
        this._queryData = data;
    }

    get queryData () {
        return this._queryData;
    }

    set postData (data) {
        this._postData = data;
    }

    get postData () {
        return this._postData;
    }

    static convertQueryToString(obj) {
        var str = [];
        for(var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    }

    _buildRequest () {
        this.url = this.url + "?" + AjaxClient.convertQueryToString(this._queryData);

        this._request.open(this._method, this.url);

        _.each(this.headers, function (headerValue, headerName) {
            //todo exception
            this._request.setRequestHeader(headerName, headerValue);
        }, this);
    }

    send () {
        this._buildRequest();
        let data = _.isEmpty(this._postData) ? null : JSON.stringify(this._postData);

        return new Promise((resolve, reject) => {
            this._request.send(data);

            this._request.onload = () => {
                var response = JSON.parse(this._request.responseText);
                if (response.success === true) {
                    resolve(response);
                } else {
                    if(response.error !== undefined) {
                        reject(response.error);
                    }
                }
                reject(Error('Unknown error'));
            }
        });
    }
}

export default AjaxClient;