Ext.define('MyshopAdmin.model.User', {
    extend: 'Ext.data.Model',

    fields: [
        {
            type: 'int',
            name: 'id'
        },
        {
            type: 'string',
            name: 'name'
        },
        {
            type: 'string',
            name: 'email'
        },
        {
            type: 'string',
            name: 'phone'
        },
        {
            type: 'string',
            name: 'ip'
        },
        {
            type: 'date',
            name: 'updated',
            mapping: 'updatedDate.date'
        },
        {
            type: 'date',
            name: 'created',
            mapping: 'createdDate.date'
        }
    ]
});
