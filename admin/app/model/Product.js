Ext.define('MyshopAdmin.model.Product', {
    extend: 'Ext.data.Model',

    fields: [
        {
            type: 'int',
            name: 'id'
        },
        {
            type: 'string',
            name: 'name'
        },
        {
            type: 'string',
            name: 'description'
        },
        {
            type: 'number',
            name: 'price'
        },
        {
            type: 'int',
            name: 'quantityOnStock'
        },
        {
            type: 'date',
            name: 'updated',
            mapping: 'updatedDate.date'
        },
        {
            type: 'date',
            name: 'created',
            mapping: 'createdDate.date'
        }
    ],

    hasMany: 'OrderProduct'
});
