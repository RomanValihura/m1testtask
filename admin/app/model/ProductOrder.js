Ext.define('MyshopAdmin.model.ProductOrder', {
    extend: 'Ext.data.Model',

    fields: [
        {
            type: 'int',
            name: 'id'
        },
        {
            type: 'string',
            name: 'userName',
            mapping: 'user.name'
        },
        {
            type: 'date',
            name: 'updated',
            mapping: 'updatedDate.date'
        },
        {
            type: 'date',
            name: 'created',
            mapping: 'createdDate.date'
        }
    ]
    ,

    hasOne: [
        {
            name: 'product',
            model: 'Product',
            associationKey: 'product'
        }
    ]
});
