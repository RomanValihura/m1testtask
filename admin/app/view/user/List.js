/**
 * This view is an example list of people.
 */
Ext.define('MyshopAdmin.view.user.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'userlist',

    requires: [
        'MyshopAdmin.store.User'
    ],

    title: 'Users',

    store: {
        type: 'user'
    },

    columns: [
        { text: 'Id',  dataIndex: 'id' },
        { text: 'Name',  dataIndex: 'name' },
        { text: 'Email', dataIndex: 'email'},
        { text: 'Phone', dataIndex: 'phone'},
        { text: 'Ip',  dataIndex: 'ip', flex: 1}
    ],

    loadMask: true,

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: {
            type: 'user'
        },
        dock: 'bottom',
        displayInfo: true
    }],

    listeners: {
        select: function (sender, record) {
            Ext.Ajax.request({
                method: 'GET',
                url: 'http://ipinfo.io/' + record.data.ip,

                headers: {
                    Accept: 'application/json'
                },

                success: function (response) {
                    var response = Ext.JSON.decode(response.responseText);
                    var location = response.loc;
                    var city = response.city;

                    var locationWindow = Ext.create('Ext.window.Window', {
                        title: 'User location',
                        height: 600,
                        width: 700,
                        layout: 'fit',
                        preventBodyReset: true,
                        html: '' +
                        '<iframe ' +
                        'width="720" ' +
                        'height="600" ' +
                        'src="http://www.maps.ie/create-google-map/map.php?width=720&amp;height=600&amp;hl=en&amp;coord=' + location + '&amp;q=' + city + '+(User%20location)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=A&amp;output=embed" ' +
                        'frameborder="0" ' +
                        'scrolling="no" ' +
                        'marginheight="0" ' +
                        'marginwidth="0">' +
                        '</iframe>'
                    });

                    locationWindow.show();
                }
            });
        }
    }
});
