/**
 * This view is an example list of people.
 */
Ext.define('MyshopAdmin.view.product.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'productlist',

    height: 500,

    requires: [
        'MyshopAdmin.store.Product'
    ],

    title: 'Products',

    store: {
        type: 'product'
    },

    columns: [
        {
            text: 'Id',
            dataIndex: 'id',
            renderer:function(value) {
                return parseInt(value, 10) > 0 ? value : '';
            }
        },
        {
            text: 'Name',
            dataIndex: 'name',
            editor: 'textfield',
            flex: 1
        },
        {
            text: 'Description',
            dataIndex: 'description',
            editor: 'textfield',
            flex: 1
        },
        {
            text: 'Price',
            dataIndex: 'price',
            editor: 'textfield',
            flex: 1
        },
        {
            text: 'Quantity on stock',
            dataIndex: 'quantityOnStock',
            editor: 'textfield',
            flex: 1
        },
        {
            text: 'Created Date',
            dataIndex: 'created',
            flex: 1
        },
        {
            text: 'Updated date',
            dataIndex: 'updated',
            flex: 1
        }
    ],

    listeners: {
        edit: function(editor) {
            editor.grid.store.sync();
        },
        afterrender: function (grid) {

        }
    },

    tbar: [{
        text: 'Add Product',
        handler : function() {
            var grid = this.up('grid');
            if (grid) {
                grid.editingPlugin.cancelEdit();

                // Create a model instance
                var product = Ext.create('MyshopAdmin.model.Product');

                grid.store.insert(0, product);
                grid.editingPlugin.startEdit(product);
            }
        }
    }, {
        text: 'Remove product',
        handler: function() {
            var grid = this.up('grid');
            if (grid) {
                var sm = grid.getSelectionModel();
                var rs = sm.getSelection();
                if (!rs.length) {
                    Ext.Msg.alert('Info', 'No Records Selected');
                    return;
                }

                Ext.Msg.confirm(
                    'Remove Product',
                    'Remove product "' + rs[0].data.name + '" ?',
                    function (button) {
                        if (button == 'yes') {
                            grid.store.remove(rs[0]);
                            grid.store.sync();
                        }
                    }
                );
            }
        }
    }],

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: {
            type: 'product'
        },
        dock: 'bottom',
        displayInfo: true
    }],

    selModel: 'rowmodel',
    plugins: {
        ptype: 'rowediting',
        clicksToEdit: 1
    }
});
