/**
 * This view is an example list of people.
 */
Ext.define('MyshopAdmin.view.order.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'orderlist',

    requires: [
        'MyshopAdmin.store.ProductOrder'
    ],

    title: 'Orders',

    store: {
        type: 'order'
    },

    columns: [
        {
            text: 'Id',
            dataIndex: 'id'
        },
        {
            text: 'User',
            dataIndex: 'userName',
            flex: 1
        },
        {
            text: 'Order Date',
            dataIndex: 'created',
            flex: 1
        }
    ],

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: {
            type: 'order'
        },
        dock: 'bottom',
        displayInfo: true
    }],

    listeners: {
        select: function (e, record) {
            var store = Ext.create('Ext.data.ArrayStore', {
                fields: [
                    {
                        type: 'string',
                        name: 'productName',
                        mapping: 'product.name'
                    },
                    {
                        type: 'string',
                        name: 'description',
                        mapping: 'product.description'
                    },
                    {
                        type: 'int',
                        name: 'quantity',
                        mapping: 'quantity'
                    },
                    {
                        type: 'number',
                        name: 'itemPrice',
                        mapping: 'product.price'
                    }
                ],
                data: record.data.orderProducts,
                expandData: true
            });

            var locationWindow = Ext.create('Ext.window.Window', {
                title: 'Order #' + record.data.id,
                height: 600,
                width: 700,
                layout: 'fit',
                preventBodyReset: true,
                items: [
                    {
                        extend: 'Ext.grid.Panel',
                        xtype: 'orderlist',

                        requires: [
                            'MyshopAdmin.store.ProductOrder'
                        ],

                        header: false,

                        store: store,

                        columns: [
                            {
                                text: 'Product name',
                                dataIndex: 'productName',

                                flex: 1
                            },
                            {
                                text: 'Product name',
                                dataIndex: 'description',
                                flex: 1
                            },
                            {
                                text: 'Quantity',
                                dataIndex: 'quantity',
                                flex: 1
                            },
                            {
                                text: 'Item price',
                                dataIndex: 'itemPrice',
                                flex: 1
                            },
                            {
                                text: 'Total price',
                                flex: 1,
                                renderer: function (value,metadata,record) {
                                    //console.log(record);
                                    return record.data.itemPrice * record.data.quantity;
                                }
                            }
                        ]
                    }
                ]
            });

            locationWindow.show();
        }.bind(this)
    }
});
