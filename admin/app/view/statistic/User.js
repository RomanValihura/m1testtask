Ext.define('MyshopAdmin.view.statistic.User', {
    extend: 'Ext.form.Panel',
    xtype: 'statisticuser',

    title: 'User statistic',
    height: 200,
    bodyPadding: 10,
    defaultType: 'textfield',
    items: [
        {
            id: 'amountOrderUser',
            xtype: 'displayfield',
            fieldLabel: 'Amount of order',
            name: 'amountOrder',
            hidden: true
        },
        {
            id: 'userEmailInput',
            fieldLabel: 'User email',
            name: 'email',
            vtype: 'email',
            listeners: {
                change: function (field) {
                    Ext.ComponentMgr.get('submitAmount').setDisabled(
                        !field.isValid()
                    );
                }
            }
        },
        {
            id: 'submitAmount',
            xtype: 'button',
            text: 'Get order amount',
            disabled: true,
            handler: function (e) {
                Ext.Ajax.request({
                    method: 'GET',
                    url: '/api/statistic/user',
                    params: {
                        email: Ext.ComponentMgr.get('userEmailInput').getValue()
                    },
                    headers: {
                        token: localStorage.getItem('token')
                    },
                    success: function(response){
                        var respone = Ext.JSON.decode(response.responseText);
                        var orderAmount = respone.result.orderAmount;

                        var resultTarget = Ext.ComponentMgr.get('amountOrderUser');
                        resultTarget.setValue(orderAmount);
                        resultTarget.show();
                    }
                });
            }
        }
    ]
});