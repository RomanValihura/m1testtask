/**
 * This view is an example list of people.
 */
Ext.define('MyshopAdmin.view.statistic.Grid', {
    extend: 'Ext.grid.Panel',
    xtype: 'statisticgrid',

    requires: [
        'MyshopAdmin.store.Statistic'
    ],

    title: 'Orders',

    store: {
        type: 'statistic'
    },

    columns: [
        {
            text: 'Total order number',
            dataIndex: 'totalOrders',
            flex: 1
        },
        {
            text: 'Total products in store',
            dataIndex: 'totalProduct',
            flex: 1
        },
        {
            text: 'Total amount of orders',
            dataIndex: 'orderAmount',
            flex: 1
        }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
