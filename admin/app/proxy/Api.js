Ext.define('MyshopAdmin.proxy.API', {
    extend: 'Ext.data.proxy.Rest',
    alias: 'proxy.api',

    headers: {
        token: localStorage.getItem('token') || ''
    },
    reader: {
        type: 'json',
        rootProperty: 'result',
        totalProperty: 'total'
    },
    writer: {
        type: 'json'
    },

    startParam: 'offset'
});