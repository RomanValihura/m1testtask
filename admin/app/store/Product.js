Ext.define('MyshopAdmin.store.Product', {
    id: 'productStore',

    extend: 'Ext.data.Store',

    alias: 'store.product',

    model: 'MyshopAdmin.model.Product',

    autoLoad: true,
    autoDestroy: true,

    proxy: {
        type: 'api',
        url: '/api/product'
    }
});