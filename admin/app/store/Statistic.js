Ext.define('MyshopAdmin.store.Statistic', {
    extend: 'Ext.data.Store',

    alias: 'store.statistic',

    fields: [
        "totalOrders",
        "totalProduct",
        "orderAmount"
    ],

    autoLoad: true,

    proxy: {
        type: 'api',
        url: '/api/statistic'
    }
});