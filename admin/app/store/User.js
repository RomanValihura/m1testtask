Ext.define('MyshopAdmin.store.User', {
    extend: 'Ext.data.Store',

    alias: 'store.user',

    model: 'MyshopAdmin.model.User',

    autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'api',
        url: '/api/user'
    }
});
