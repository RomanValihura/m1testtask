Ext.define('MyshopAdmin.store.ProductOrder', {
    extend: 'Ext.data.Store',

    alias: 'store.order',

    model: 'MyshopAdmin.model.ProductOrder',

    autoLoad: true,

    proxy: {
        type: 'api',
        url: '/api/product_order'
    }
});