var lineconfig = {
    xtype: 'box',
    autoEl:{
        tag: 'div',
        style:'margin-bottom: 30px',
    }
};

Ext.define('MyshopAdmin.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',

        'MyshopAdmin.proxy.API',
        'MyshopAdmin.view.main.MainController',
        'MyshopAdmin.view.main.MainModel',
        'MyshopAdmin.view.user.List',
        'MyshopAdmin.view.product.List',
        'MyshopAdmin.view.order.List',
        'MyshopAdmin.view.statistic.Grid',
        'MyshopAdmin.view.statistic.User'
    ],

    controller: 'main',
    viewModel: 'main',

    ui: 'navigation',

    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list'
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 20,
        tabConfig: {
            plugins: 'responsive',
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },

    items: [{
        title: 'Statistic',
        iconCls: 'fa-area-chart',
        items: [
            {
                xtype: 'statisticgrid'
            },
            lineconfig,
            {
                xtype: 'statisticuser'
            }
        ]
    }, {
        title: 'Users',
        iconCls: 'fa-users',
        items: [{
            xtype: 'userlist'
        }]
    }, {
        title: 'Products',
        iconCls: 'fa-shopping-cart',
        items: [{
            xtype: 'productlist'
        }]
    }, {
        title: 'Orders',
        iconCls: 'fa-credit-card',
        items: [{
            xtype: 'orderlist'
        }]
    }]
});
